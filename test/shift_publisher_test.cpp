#include <memory>

// GTest headers
#include <gtest/gtest.h>

// ROS headers
#include "ament_index_cpp/get_package_share_directory.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "rclcpp/rclcpp.hpp"
#include "rclcpp/subscription_base.hpp"
#include "rclcpp/utilities.hpp"
#include "sensor_msgs/msg/nav_sat_fix.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include "sensor_msgs/msg/time_reference.hpp"
#include "tf2_msgs/msg/tf_message.hpp"

// Osef headers
#include "tlvParser.h"

// Local headers
#include "outsight_shift_driver/msg/augmented_cloud.hpp"
#include "outsight_shift_driver/msg/tracked_objects.hpp"
#include "shift_common.h"
#include "shift_publisher.h"

// Constant definition of test data.
constexpr const char *k_package_name = "/outsight_shift_driver"; // TODO improve
constexpr const char *k_resources = "/resources";
constexpr const char *k_shift_tracking_file = "/shift_tracking_mode_record.osef";
constexpr const char *k_shift_slam_file = "/shift_slam_mode_record.osef";
constexpr const char *k_shift_gps_file = "/shift_mobile_gps_record.osef";
constexpr const char *k_default_shift_base_frame_id = "base_frame_id";
constexpr const char *k_default_shift_map_frame_id = "map_frame_id";

constexpr const size_t k_tracking_messages = 61;
constexpr const size_t k_slam_messages = 55;
constexpr const size_t k_gps_messages = 24;

/// Class to test the ShiftPublisher topics.
class ShiftPublisherTest : public ::testing::Test {
    protected:
	void SetUp() override
	{
		node = rclcpp::Node::make_shared("ShiftPublisherTest");
		shift_publisher = std::make_shared<ShiftPublisher>(node);

		node->set_parameter({ ALBCommon::k_shift_base_frame_id, k_default_shift_base_frame_id });
		node->set_parameter({ ALBCommon::k_shift_map_frame_id, k_default_shift_map_frame_id });
	}

	void TearDown() override
	{
		// Free memory.
		if (file) {
			fclose(file);
		}
		delete (buffer);
	}

	/// \brief Open testing file.
	void openFile(const std::string &key)
	{
		const std::string package_share_directory =
			ament_index_cpp::get_package_share_directory(k_package_name);
		std::string filename = package_share_directory + k_resources + key;
		file = fopen(filename.c_str(), "r");
	}

	/// \brief Subscribe to the given topic with the template ROS message.
	template <typename ROSMessageType>
	void subscribeTo(const std::string &topic)
	{
		shift_subscriber = node->create_subscription<ROSMessageType>(
			topic, 1000,
			std::bind(&ShiftPublisherTest::onMessageReceived<ROSMessageType>, this, std::placeholders::_1));
	}

	/// \brief Callback function on the subscriber->
	template <typename ROSMessageType>
	void onMessageReceived(const typename ROSMessageType::ConstPtr &)
	{
		received_messages++;
	}

	/// \brief Function to read all data and publish them.
	void readAndPublish()
	{
		// Read and publish TLV data.
		Tlv::tlv_s *read_tlv = nullptr;
		rclcpp::Rate loop_rate(100);

		while ((read_tlv = Tlv::readFromFile(file, buffer, buffer_size))) {
			shift_publisher->publish(read_tlv);
			rclcpp::spin_some(node);
			loop_rate.sleep();
		}
	}

    protected:
	FILE *file = nullptr;
	uint8_t *buffer = nullptr;
	size_t buffer_size = 0;
	size_t received_messages = 0;

	std::shared_ptr<rclcpp::Node> node;
	std::shared_ptr<rclcpp::SubscriptionBase> shift_subscriber;
	std::shared_ptr<ShiftPublisher> shift_publisher;
};

/// \brief Test to check opening the tracking testing file.
TEST_F(ShiftPublisherTest, openTrackingFile)
{
	openFile(k_shift_tracking_file);
	ASSERT_FALSE(!file);
}

/// \brief Test to check opening the slam testing file.
TEST_F(ShiftPublisherTest, openSlamFile)
{
	openFile(k_shift_slam_file);
	ASSERT_FALSE(!file);
}

/// \brief Test to check opening of GPS testing file.
TEST_F(ShiftPublisherTest, openGpsFile)
{
	openFile(k_shift_gps_file);
	ASSERT_FALSE(!file);
}

// \brief Test to check the PointCloud publisher.
TEST_F(ShiftPublisherTest, publishPointCloud)
{
	// Open slam file.
	openFile(k_shift_slam_file);
	ASSERT_FALSE(!file);

	// Set parameter to publish PointCloud message.
	node->set_parameter({ ALBCommon::k_shift_point_cloud_config, true });
	shift_publisher->init();

	subscribeTo<sensor_msgs::msg::PointCloud2>(ALBCommon::k_shift_point_cloud);

	EXPECT_EQ(shift_subscriber->get_publisher_count(), 1);

	readAndPublish();
	EXPECT_EQ(received_messages, k_slam_messages);
}

// \brief Test to check the Pose publisher.
TEST_F(ShiftPublisherTest, publishPose)
{
	// Open tracking file.
	openFile(k_shift_slam_file);
	ASSERT_FALSE(!file);

	// Set parameter to publish Pose message.
	node->set_parameter({ ALBCommon::k_shift_pose_config, true });
	shift_publisher->init();

	subscribeTo<geometry_msgs::msg::PoseStamped>(ALBCommon::k_shift_pose);

	EXPECT_EQ(shift_subscriber->get_publisher_count(), 1);

	readAndPublish();
	EXPECT_EQ(received_messages, k_slam_messages);
}

// \brief Test to check the TimeReference publisher.
TEST_F(ShiftPublisherTest, publishTimeReference)
{
	// Open tracking file.
	openFile(k_shift_tracking_file);
	ASSERT_FALSE(!file);

	// Set parameter to publish TimeReference message.
	node->set_parameter({ ALBCommon::k_shift_time_ref_config, true });
	shift_publisher->init();

	subscribeTo<sensor_msgs::msg::TimeReference>(ALBCommon::k_shift_time_ref);

	EXPECT_EQ(shift_subscriber->get_publisher_count(), 1);

	readAndPublish();
	EXPECT_EQ(received_messages, k_tracking_messages);
}

// \brief Test to check the TrackedObjects publisher.
TEST_F(ShiftPublisherTest, publishTrackedObjects)
{
	// Open tracking file.
	openFile(k_shift_tracking_file);
	ASSERT_FALSE(!file);

	// Set parameter to publish TrackedObjects message.
	node->set_parameter({ ALBCommon::k_shift_objects_config, true });
	shift_publisher->init();

	subscribeTo<outsight_shift_driver::msg::TrackedObjects>(ALBCommon::k_shift_objects);

	EXPECT_EQ(shift_subscriber->get_publisher_count(), 1);

	readAndPublish();
	EXPECT_EQ(received_messages, k_tracking_messages);
}

// \brief Test to check the Egomotion publisher.
TEST_F(ShiftPublisherTest, publishEgomotion)
{
	// Open tracking file.
	openFile(k_shift_tracking_file);
	ASSERT_FALSE(!file);

	// Set parameter to publish Egomotion message.
	node->set_parameter({ ALBCommon::k_shift_egomotion_config, true });
	shift_publisher->init();

	subscribeTo<geometry_msgs::msg::PoseStamped>(ALBCommon::k_shift_egomotion);

	EXPECT_EQ(shift_subscriber->get_publisher_count(), 1);

	readAndPublish();
	EXPECT_EQ(received_messages, k_tracking_messages);
}

// \brief Test to check the Odometry publisher.
TEST_F(ShiftPublisherTest, publishOdometry)
{
	// Open tracking file.
	openFile(k_shift_tracking_file);
	ASSERT_FALSE(!file);

	// Set parameter to publish Odometry message.
	node->set_parameter({ ALBCommon::k_shift_odometry_config, true });
	shift_publisher->init();

	subscribeTo<nav_msgs::msg::Odometry>(ALBCommon::k_shift_odometry);

	EXPECT_EQ(shift_subscriber->get_publisher_count(), 1);

	readAndPublish();
	EXPECT_EQ(received_messages, k_tracking_messages);
}

// \brief Test to check the AugmentedCloud publisher.
TEST_F(ShiftPublisherTest, publishAugmentedCloud)
{
	// Open slam file.
	openFile(k_shift_slam_file);
	ASSERT_FALSE(!file);

	// Set parameter to publish PointCloud message.
	node->set_parameter({ ALBCommon::k_shift_augmented_cloud_config, true });
	shift_publisher->init();

	subscribeTo<outsight_shift_driver::msg::AugmentedCloud>(ALBCommon::k_shift_augmented_cloud);

	EXPECT_EQ(shift_subscriber->get_publisher_count(), 1);

	readAndPublish();
	EXPECT_EQ(received_messages, k_slam_messages);
}

// \brief Test to check the GPS publisher.
TEST_F(ShiftPublisherTest, publishGps)
{
	// Open slam file.
	openFile(k_shift_gps_file);
	ASSERT_FALSE(!file);

	// Set parameter to publish PointCloud message.
	node->set_parameter({ ALBCommon::k_shift_gps_pose_config, true });
	shift_publisher->init();

	subscribeTo<sensor_msgs::msg::NavSatFix>(ALBCommon::k_shift_gps_pose);

	EXPECT_EQ(shift_subscriber->get_publisher_count(), 1);

	readAndPublish();
	EXPECT_EQ(received_messages, k_gps_messages);
}

// \brief Test to check the odom to lidar tf broadcaster.
TEST_F(ShiftPublisherTest, broadcastShiftTransform)
{
	// Open slam file.
	openFile(k_shift_slam_file);
	ASSERT_FALSE(!file);

	// Set parameter to publish Shift fixed frame to sensor frame tf.
	node->set_parameter({ ALBCommon::k_shift_broadcast_tf_config, true });
	node->set_parameter({ ALBCommon::k_shift_odometry_config, true });
	shift_publisher->init();

	subscribeTo<tf2_msgs::msg::TFMessage>("tf");

	readAndPublish();
	EXPECT_EQ(received_messages, k_slam_messages);
}

// \brief Test to check the ZoneBindings publisher.
TEST_F(ShiftPublisherTest, publishZoneBindings)
{
	// Open tracking file.
	openFile(k_shift_tracking_file);
	ASSERT_FALSE(!file);

	// Set parameter to publish ZoneBindings message.
	node->set_parameter({ ALBCommon::k_shift_zone_bindings_config, true });
	shift_publisher->init();

	subscribeTo<outsight_shift_driver::msg::ZoneBindings>(ALBCommon::k_shift_zone_bindings);

	EXPECT_EQ(shift_subscriber->get_publisher_count(), 1);

	readAndPublish();
	EXPECT_EQ(received_messages, k_tracking_messages);
}

// \brief Test to check the Zones publisher.
TEST_F(ShiftPublisherTest, publishZones)
{
	// Open tracking file.
	openFile(k_shift_tracking_file);
	ASSERT_FALSE(!file);

	// Set parameter to publish Zones message.
	node->set_parameter({ ALBCommon::k_shift_zones_config, true });
	shift_publisher->init();

	subscribeTo<outsight_shift_driver::msg::Zones>(ALBCommon::k_shift_zones);

	EXPECT_EQ(shift_subscriber->get_publisher_count(), 1);

	readAndPublish();
	EXPECT_EQ(received_messages, k_tracking_messages);
}

int main(int argc, char **argv)
{
	rclcpp::init(0, nullptr);
	testing::InitGoogleTest(&argc, argv);
	auto res = RUN_ALL_TESTS();
	rclcpp::shutdown();
	return res;
}
