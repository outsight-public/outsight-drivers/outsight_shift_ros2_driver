// Standard headers
#include <memory>
#include <thread>

// GTest headers
#include <gtest/gtest.h>

// ROS headers
#include "rclcpp/rclcpp.hpp"
#include "std_srvs/srv/trigger.hpp"

// Local headers
#include "shift_common.h"
#include "shift_requester.h"

#include "outsight_shift_driver/srv/shift_config.hpp"
#include "outsight_shift_driver/srv/shift_file.hpp"

// Constant testing definition.
constexpr const char *k_invalid_service = "invalid_service";

/// Class to test the ALB services.
class ShiftServicesTest : public ::testing::Test {
    protected:
	void SetUp() override
	{
		node = rclcpp::Node::make_shared("ShiftServicesTest");
		requester = std::make_unique<ShiftRequester>(node);
	}

	/// \brief Function to check if a service can be called.
	template <typename service_t>
	void testServiceCall(const std::string &service_name)
	{
		auto request = std::make_shared<typename service_t::Request>();
		auto client = node->create_client<service_t>(service_name);
		// Check that the callback is called.
		ASSERT_TRUE(client->wait_for_service(std::chrono::seconds(1)));
		EXPECT_NO_THROW(auto response = client->async_send_request(request));
	}

	bool checkIfServiceExists(const std::string &service_name)
	{
		auto available_services = node->get_service_names_and_types();
		return static_cast<bool>(available_services.count("/" + service_name)); // TODO tmp investigate
	}

    protected:
	std::shared_ptr<rclcpp::Node> node;
	std::unique_ptr<ShiftRequester> requester;
};

// \brief Test to check that invalid service call should fail.
TEST_F(ShiftServicesTest, invalidService)
{
	ASSERT_FALSE(checkIfServiceExists(k_invalid_service));
	auto request = std::make_shared<std_srvs::srv::Trigger>();
	auto client = node->create_client<std_srvs::srv::Trigger>(k_invalid_service);
	ASSERT_FALSE(client->wait_for_service(std::chrono::seconds(1)));
}

/// \brief Test if the processing services are defined.
TEST_F(ShiftServicesTest, processingServicesDefined)
{
	ASSERT_TRUE(checkIfServiceExists(ALBCommon::k_shift_processing_restart));
	ASSERT_TRUE(checkIfServiceExists(ALBCommon::k_shift_processing_stop));
	ASSERT_TRUE(checkIfServiceExists(ALBCommon::k_shift_processing_get_config));
	ASSERT_TRUE(checkIfServiceExists(ALBCommon::k_shift_processing_put_config));
}

/// \brief Test if the processing services are called.
TEST_F(ShiftServicesTest, processingServicesCalled)
{
	testServiceCall<std_srvs::srv::Trigger>(ALBCommon::k_shift_processing_restart);
	testServiceCall<std_srvs::srv::Trigger>(ALBCommon::k_shift_processing_stop);
}

/// \brief Test if the processing configuration services are called.
TEST_F(ShiftServicesTest, processingConfigServicesCalled)
{
	testServiceCall<outsight_shift_driver::srv::ShiftConfig>(ALBCommon::k_shift_processing_get_config);
	testServiceCall<outsight_shift_driver::srv::ShiftConfig>(ALBCommon::k_shift_processing_put_config);
}

// \brief Test if the storage services are defined.
TEST_F(ShiftServicesTest, storageServicesDefined)
{
	ASSERT_TRUE(checkIfServiceExists(ALBCommon::k_shift_storage_download));
	ASSERT_TRUE(checkIfServiceExists(ALBCommon::k_shift_storage_upload));
	ASSERT_TRUE(checkIfServiceExists(ALBCommon::k_shift_storage_list));
}

/// \brief Test if the storage services are called.
TEST_F(ShiftServicesTest, storageServicesCalled)
{
	testServiceCall<outsight_shift_driver::srv::ShiftFile>(ALBCommon::k_shift_storage_download);
	testServiceCall<outsight_shift_driver::srv::ShiftFile>(ALBCommon::k_shift_storage_upload);
	testServiceCall<outsight_shift_driver::srv::ShiftFile>(ALBCommon::k_shift_storage_list);
}

int main(int argc, char **argv)
{
	rclcpp::init(0, nullptr);
	testing::InitGoogleTest(&argc, argv);

	auto res = RUN_ALL_TESTS();

	rclcpp::shutdown();
	return res;
}
