^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package outsight_shift_driver
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

2.3.1 (2024-05-27)
------------------

Fixed
=====
* put shift georef heading back in ENU

2.3.0 (2024-05-13)
------------------

Added
=====
* support ROS2 Humble

2.2.1 (2024-02-20)
------------------

Fixed
=====
* Fix georeferenced heading parsing.

2.2.0 (2024-01-25)
------------------

Added
=====
* zones parsing from the OSEF. A toggle allow to switch back to a curl query.

2.1.1 (2024-01-11)
------------------

Fixed
=====
* CI now properly fails if a UT fails
* Fixed ALS pose lazy parsing


2.1.0 (2024-01-09)
------------------

Added
=====
* zone bindings parsing 
* georeferenced pose/speed parsing 
* Endlessly try to reconnect to Shift even if the init fails
* Added a restart mechanism in case of a long deconnection
* Now catching all exceptions and relaunching the processing

Changed
=======

* Update OSEF types to 1.14.0
* Only parsing ALS Pose when needed


2.0.0 (2023-12-05)
------------------

Changed
=======

Migration to ROS2 on progress


1.6.0 (2022-10-11)
------------------
Added
=======
* Configuration file parameter to roslaunch command

Changed
=======
* Update OSEF types to 1.6.1

Fixed
=======
* Fix TrackedObjects log (`MR #1 <https://gitlab.com/outsight-public/outsight-drivers/outsight_shift_driver/-/merge_requests/1>`__)


1.5.1 (2022-06-28)
------------------
Fixed
=======
* Fix odometry frame ID
* Always publish transform between map and fixed frame, and fix Pose frame

1.5.0 (2022-06-23)
------------------
Added
=======
* Publish zone definitions

Changed
=======
* Update OSEF types to 1.3.0

Fixed
=======
* Fix Odometry computation
* Use egomotion directly from ALB output

1.4.0 (2022-05-04)
------------------
Added
=======
* Add configuration parameter to reorder PointCloud2 to row-major order

Fixed
=======
* Fix dependency on navigation messages
* Fix computation of `row_step` for PointCloud2 message

1.3.0 (2022-04-29)
------------------
Added
=======
* Odometry publisher

Changed
=======
* AugmentedCloud message update
* Update OSEF library v1.2.1
* Frame system improved for robotic integration

Fixed
=======
* Use private node for parameters (`Issue #4 <https://gitlab.com/outsight-public/outsight-drivers/outsight_shift_driver/-/issues/4>`__)
* Fix file data reading in ShiftCurlHelper
* Fix file uploading detected on ROS Melodic (`Issue #2 <https://gitlab.com/outsight-public/outsight-drivers/outsight_shift_driver/-/issues/2>`__)
* Fix checking defined storage command

1.2.2 (2022-03-14)
------------------
Fixed
=====
* Timereference microseconds conversion

1.2.1 (2022-02-10)
------------------
Fixed
=====
* Define fix loop rate at 20hz for services (`Issue #3 <https://gitlab.com/outsight-public/outsight-drivers/outsight_shift_driver/-/issues/3>`__)

1.2.0 (2021-12-9)
------------------
Added
=====
* Publish Outsight AugmentedCloud
* Add ROS documentation links

Changed
=======
* Update OSEF library

1.1.0 (2021-11-18)
------------------
Added
=====
* Add service to list available files on the ALB
* Add LaserScan conversion

Fixed
=====
* Fix normalized quaternion

1.0.0 (2021-11-2)
------------------
Added
=====
* First public release
* Node to parse messages from the ALB

  * PointCloud
  * Pose
  * TrackedObjects

* Services to interact with the ALB

  * Start/Stop processing
  * Get/Put configuration of the ALB
  * Upload/Download files on the ALB
