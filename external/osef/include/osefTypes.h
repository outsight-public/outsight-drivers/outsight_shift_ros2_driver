#pragma once

#include <stdint.h>

// !!! Warning !!!
// !!! This file has been auto generated, do not attempt to edit it !!!
// Important: all words use little-endian representation, unless specified otherwise

// Augmented Cloud
// **Output when**: Augmented Cloud is enabled.
// **Purpose**: An augmented cloud represents a cloud of points. Each point can have attributes, all of which are
// optional, which describe its position, physical property, movement, etc...
// Contains following sub-TLVs:
// - exactly one:     Number of Points               (OSEF_TYPE_NUMBER_OF_POINTS)
// - exactly one:     Number of Layers               (OSEF_TYPE_NUMBER_OF_LAYERS)
// - none or one:     Spherical Coordinates          (OSEF_TYPE_SPHERICAL_COORDINATES) [DEPRECATED USAGE]
// - none or one:     Reflectivities                 (OSEF_TYPE_REFLECTIVITIES)
// - none or one:     Cartesian Coordinates          (OSEF_TYPE_CARTESIAN_COORDINATES)
// - none or one:     Object ID 32 Bits              (OSEF_TYPE_OBJECT_ID_32_BITS)
// - none or one:     Cartesian Coordinates 4F       (OSEF_TYPE_CARTESIAN_COORDINATES_4F) [DEPRECATED USAGE]
// - none or one:     Reference Map Bits             (OSEF_TYPE_REFERENCE_MAP_BITS)
// - none or one:     Coordinates Reference System   (OSEF_TYPE_COORDINATES_REFERENCE_SYSTEM)
#define OSEF_TYPE_AUGMENTED_CLOUD 1UL

// Number of Points
// **Output when**: Augmented Cloud is enabled.
// **Format**: Contains 32 bits unsigned int value.
// **Purpose**: Represents the number of points in the point cloud.
#define OSEF_TYPE_NUMBER_OF_POINTS 2UL

// Spherical Coordinates [DEPRECATED USAGE]
// Contains list of spherical single-precision float coordinates, three per point:
// * azimuth : degrees, range [ 0.0 .. 360.0 ]
// * elevation : degrees, range [ -90.0 .. +90.0 ]
// * distance : meters, range [ 0.0 ..  +inf ]
#define OSEF_TYPE_SPHERICAL_COORDINATES 3UL

// Reflectivities
// **Output when**: Augmented Cloud is enabled.
// **Format**: List of unsigned 8-bits integers.
// **Purpose**: Contains list of reflectivity values.
// Diffuse reflectors report values from 0 to 100 for reflectivities from 0% to 100%.
// Retroreflectors report values from 101 to 255, where 255 represents an ideal reflection.
#define OSEF_TYPE_REFLECTIVITIES 4UL

// Cartesian Coordinates
// **Output when**: Augmented Cloud is enabled.
// Contains list of Cartesian single-precision float coordinates, three per point:
// * x : meters, range [ -inf .. +inf ]
// * y : meters, range [ -inf .. +inf ]
// * z : meters, range [ -inf .. +inf ]
// **Coordinate Frame**:
// - Passthrough & Mobile: Relative to LiDAR Coordinate Frame.
// - Static: in Absolute/World Frame, which is:
//     - Single-LiDAR: On-Ground LiDAR Coordinate Frame.
//     - Multi-LiDARs: Map Coordinate Frame.
#define OSEF_TYPE_CARTESIAN_COORDINATES 6UL

// Number of Objects
// **Output when**: Tracking is enabled.
// **Purpose**: Number of tracked objects.
// **Format**: Unsigned 32-bits integer, range [ 0 .. 2^32 [
#define OSEF_TYPE_NUMBER_OF_OBJECTS 10UL

// Timestamp Microsecond
// **Output when**: Always.
// **Purpose**: Describes a unix timestamp with microsecond precision, same as struct timeval of UNIX <sys/time.h>.
// **Format**: Contains concatenation of:
// * UNIX time in seconds,   unsigned 32-bits integer, range [ 0 .. 2^32 [
// * remaining microseconds, unsigned 32-bits integer, range [ 0 .. 1000000 [
#define OSEF_TYPE_TIMESTAMP_MICROSECOND 12UL

// Number of Layers
// **Output when**: Augmented Cloud is enabled.
// **Format**: Contains 32 bits unsigned int value.
// **Purpose**: Represents the number of layers of the point cloud.
// 0 indicates that this cloud does not have a 2D structure.
#define OSEF_TYPE_NUMBER_OF_LAYERS 14UL

// Class ID Array
// **Output when**: Tracking is enabled.
// **Format**: List of signed 32 bits integers, using enum CLASS ID.
// **Purpose**: Contains list of class IDs. For further details, see:
// [Classification](https://docs.outsight.ai/software/understanding-osef-data/object-tracking-data#classification)
#define OSEF_TYPE_CLASS_ID_ARRAY 18UL

// Timestamped Data
// **Output when**: Always.
// Contains following sub-TLVs:
// - exactly one:     Timestamp Microsecond          (OSEF_TYPE_TIMESTAMP_MICROSECOND)
// - none or one:     Scan Frame                     (OSEF_TYPE_SCAN_FRAME)
#define OSEF_TYPE_TIMESTAMPED_DATA 20UL

// Pose
// **Output when**: Slam is enabled.
// **Purpose**: Pose of the LiDAR.
// **Format**: Concatenation of 12 floats of 32-bits each, in this order:
// - Tx, Ty and Tz, representing the translation vector T to get the position coordinates in meters
// - Rxx, Ryx, Rzx, Rxy, Ryy, Rzy, Rxz, Ryz and Rzz represent the rotation matrix R (given column-wise) defining the
// orientation
// As a consequence, considering a device in a pose defined by T and R, you can compute the coordinates Xabs in the
// absolute referential from the coordinates Xrel in device referential using the vectorial formula:
// ```
// Xabs = R * Xrel + T
// ```
// with:
// ```
//     [Rxx Rxy Rxz]
// R = [Ryx Ryy Ryz]
//     [Rzx Rzy Rzz]
//     [Tx]
// T = [Ty]
//     [Tz]
// ```
// **Coordinate Frame**:
// Absolute/World Frame, which is:
// - Ego-Motion: Initial pose of the LiDAR.
// - Relocalization: Map Coordinate Frame.
#define OSEF_TYPE_POSE 24UL

// Scan Frame
// **Output when**: Always.
// Contains following sub-TLVs:
// - none or one:     Augmented Cloud                (OSEF_TYPE_AUGMENTED_CLOUD)
// - none or one:     Pose                           (OSEF_TYPE_POSE)
// - none or one:     Geographic Pose                (OSEF_TYPE_GEOGRAPHIC_POSE) [DEPRECATED USAGE]
// - none or one:     Geographic Pose Precise        (OSEF_TYPE_GEOGRAPHIC_POSE_PRECISE)
// - none or one:     Geographic Speed               (OSEF_TYPE_GEOGRAPHIC_SPEED)
// - none or one:     Ego Motion                     (OSEF_TYPE_EGO_MOTION)
// - none or one:     Tracked Objects                (OSEF_TYPE_TRACKED_OBJECTS)
// - none or one:     Zones Def                      (OSEF_TYPE_ZONES_DEF)
// - none or one:     Zones Objects Binding          (OSEF_TYPE_ZONES_OBJECTS_BINDING) [DEPRECATED USAGE]
// - none or one:     Zones Objects Binding 32 Bits  (OSEF_TYPE_ZONES_OBJECTS_BINDING_32_BITS)
#define OSEF_TYPE_SCAN_FRAME 25UL

// Tracked Objects
// **Output when**: Tracking is enabled.
// **Purpose**: Properties of tracked objects, which includes centroid, bbox, speed...
// For further details, see: [Object Tracking
// Data](https://docs.outsight.ai/software/understanding-osef-data/object-tracking-data).
// Contains following sub-TLVs:
// - exactly one:     Number of Objects              (OSEF_TYPE_NUMBER_OF_OBJECTS)
// - exactly one:     Object ID 32 Bits              (OSEF_TYPE_OBJECT_ID_32_BITS)
// - exactly one:     Class ID Array                 (OSEF_TYPE_CLASS_ID_ARRAY)
// - exactly one:     Bbox Sizes                     (OSEF_TYPE_BBOX_SIZES)
// - exactly one:     Speed Vectors                  (OSEF_TYPE_SPEED_VECTORS)
// - exactly one:     Pose Array                     (OSEF_TYPE_POSE_ARRAY)
// - none or one:     Slam Pose Array                (OSEF_TYPE_SLAM_POSE_ARRAY)
// - exactly one:     Object Properties              (OSEF_TYPE_OBJECT_PROPERTIES)
// - none or one:     Geographic Pose Array          (OSEF_TYPE_GEOGRAPHIC_POSE_ARRAY)
// - none or one:     Geographic Speed Array         (OSEF_TYPE_GEOGRAPHIC_SPEED_ARRAY)
#define OSEF_TYPE_TRACKED_OBJECTS 26UL

// Bbox Sizes
// **Output when**: Tracking is enabled.
// **Purpose**: Define bounding boxes around each tracked object.
// The bounding box is extended/cropped in a way to always touch the ground along the z axis.
// A bounding box is defined by its 3 dimensions, on x, y, z axes, centered on Pose Array.
// **Format**: Array of bounding box sizes, three single-precision floats per object:
// * x : meters, range [ -inf .. +inf ]
// * y : meters, range [ -inf .. +inf ]
// * z : meters, range [ -inf .. +inf ]
#define OSEF_TYPE_BBOX_SIZES 27UL

// Speed Vectors
// **Output when**: Tracking is enabled.
// **Purpose**: Speed vectors, one for each tracked object.
// Speed vectors are defined on x, y, z axes, centered on Pose Array.
// **Format**: Array of speed vectors, three single-precision floats per object:
// * x : meters per second, range [ -inf .. +inf ]
// * y : meters per second, range [ -inf .. +inf ]
// * z : meters per second, range [ -inf .. +inf ]
#define OSEF_TYPE_SPEED_VECTORS 28UL

// Pose Array
// **Output when**: Tracking is enabled.
// **Purpose**: Contains the poses defining the bounding box of a tracked object.
// **Format**: 12 floats of 32-bits per object:
// * Tx, Ty and Tz represent the translation vector T to get the position coordinates in meters.
// * Rxx, Rxy, Rxz, Ryx, Ryy, Ryz, Rzx, Rzy and Rzz represent the rotation matrix R defining the orientation.
// As a consequence, considering a device in a pose defined by T and R, you can compute the coordinates Xabs in the
// absolute referential from the coordinates Xrel in device referential using the vectorial formula:
// Xabs = R * Xrel + T
// **Coordinate Frame**:
// Absolute/World Frame, which is:
// - Single-LiDAR configuration: On-Ground LiDAR Coordinate Frame.
// - Multi-LiDARs configuration: Map Coordinate Frame.
#define OSEF_TYPE_POSE_ARRAY 29UL

// Object ID [DEPRECATED USAGE]
// **Instead, use**: "Object ID 32 bits" (Type 47)
#define OSEF_TYPE_OBJECT_ID 30UL

// Cartesian Coordinates 4F [DEPRECATED USAGE]
// **Instead, use**: "Cartesian Coordinates" (Type 6)
// Alternative way to represent coordinates, where a fourth float is added.
// It can be more efficient to construct if an application aligns the points to use SIMD on 128 bits words.
// Contains list of cartesian single-precision float coordinates, four per point:
// * x : meters, range [ -inf .. +inf ]
// * y : meters, range [ -inf .. +inf ]
// * z : meters, range [ -inf .. +inf ]
// * w : unused, for 128 bits alignment only
#define OSEF_TYPE_CARTESIAN_COORDINATES_4F 31UL

// Spherical Coordinates 4F [DEPRECATED USAGE]
// Alternative way to represent coordinates, where a fourth float is added.
// It can be more efficient to construct if an application aligns the points to use SIMD on 128 bits words.
// Contains list of spherical single-precision float coordinates, four per point:
// * azimuth   : degrees, range [ 0.0 .. 360.0 ]
// * elevation : degrees, range [ -90.0 .. +90.0 ]
// * distance  : meters,  range [ 0.0 ..  +inf ]
// * w         : unused, for 128 bits alignment only
#define OSEF_TYPE_SPHERICAL_COORDINATES_4F 32UL

// Zones Def
// **Output when**: Tracking is enabled, and at least a zone is defined.
// **Purpose**: Definition of the event zones. They represent spatial areas of interest.
// Their order is important, since the index is used to identify a zone in type 48.
// Contains following sub-TLVs:
// - zero to many:    Zone                           (OSEF_TYPE_ZONE)
#define OSEF_TYPE_ZONES_DEF 33UL

// Zone
// **Output when**: Tracking is enabled, and at least a zone is defined.
// **Purpose**: Defines one zone.
// Contains following sub-TLVs:
// - exactly one:     Zone Vertices                  (OSEF_TYPE_ZONE_VERTICES)
// - exactly one:     Zone Name                      (OSEF_TYPE_ZONE_NAME)
// - none or one:     Zone Vertical Limits           (OSEF_TYPE_ZONE_VERTICAL_LIMITS)
#define OSEF_TYPE_ZONE 34UL

// Zone Vertices
// **Output when**: Tracking is enabled, and a zone has been defined.
// **Purpose**: Vertices of the polygon defining the zone.
// They are defined on the ground, so the z coordinate is absent.
// **Format**: Contains list of cartesian single-precision float coordinates, two per point.
// * x : meters, range [ -inf .. +inf ]
// * y : meters, range [ -inf .. +inf ]
// There must be at least 3 vertices, so at least 6 floats.
#define OSEF_TYPE_ZONE_VERTICES 35UL

// Zone Name
// **Output when**: Tracking is enabled, and at least a zone is defined.
// **Purpose**: User-defined name to the zone, do not use as unique identifier.
// **Format**: It is UTF-8 encoded and null-terminated.
#define OSEF_TYPE_ZONE_NAME 36UL

// Zones Objects Binding [DEPRECATED USAGE]
// **Instead, use**: "Zones Objects Binding 32 bits" (Type 48)
// Concatenation of 0 to N couples of:
// * an unsigned 64-bits integer, representing the ID of an object (see type 30)
// * an unsigned 32-bits integer, representing the index of the zone (the n-th type 34 of type 33)
// Each object-zone couple means that the object is considered by the algorithm to be in the zone.
#define OSEF_TYPE_ZONES_OBJECTS_BINDING 38UL

// Object Properties
// **Output when**: Tracking is enabled.
// **Purpose**: Properties of the object.
// **Format**: One byte per object, each bit of this can represent different properties.
// * 1st bit: 1 if the object has a proper orientation (like a cuboid), 0 otherwise (like a cylinder).
// * 2nd bit: 1 if the object has been seen in the last scan, 0 if it was not seen.
// * 3rd bit: 1 if the object has a valid slam pose, 0 otherwise. See type 55 (Slam Pose Array) for more information.
// * 4th bit: 1 if the object is static (e.g. traffic signs, poles...), 0 otherwise
// * 5th bit: reserved for later use
// * 6th bit: reserved for later use
// * 7th bit: reserved for later use
// * 8th bit: reserved for later use
#define OSEF_TYPE_OBJECT_PROPERTIES 39UL

// Pose Relative
// **Output when**: Slam is enabled.
// **Purpose**: This pose represents the movement of the device between two scans.
// So the position of the current scan can be computed from the one of the previous scan using the following vectorial
// formula:
// ```
// Xcurrent = R * Xprevious + T
// ```
// with:
// ```
//     [Rxx Rxy Rxz]
// R = [Ryx Ryy Ryz]
//     [Rzx Rzy Rzz]
//     [Tx]
// T = [Ty]
//     [Tz]
// ```
// **Format**: Concatenation of 12 floats of 32-bits each, in this order:
// - Tx, Ty and Tz, representing the translation vector T to get the position coordinates in meters
// - Rxx, Ryx, Rzx, Rxy, Ryy, Rzy, Rxz, Ryz and Rzz, representing the rotation matrix R (given column-wise) defining
// the orientation
// **Coordinate Frame**: relative to the pose of the previous scan.
#define OSEF_TYPE_POSE_RELATIVE 42UL

// Ego Motion
// **Output when**: SLAM is enabled.
// Contains following sub-TLVs:
// - exactly one:     Pose Relative                  (OSEF_TYPE_POSE_RELATIVE)
// - none or one:     Smoothed Pose                  (OSEF_TYPE_SMOOTHED_POSE)
// - none or one:     Divergence Indicator           (OSEF_TYPE_DIVERGENCE_INDICATOR)
#define OSEF_TYPE_EGO_MOTION 44UL

// Geographic Pose [DEPRECATED USAGE]
// **Deprecated since**: 5.3
// **Instead, use**: "Geographic Pose Precise" (Type 57)
// **Purpose**: Represents the geographic pose, output from the relocalization processing, expressed in decimal
// degrees notation (latitude, longitude & heading).
// **Format**: Concatenation of 3 single precision (32 bits) floats [lat, long, heading].
// * Single-precision floating-point (float32) : latitude in decimal degrees, range [ -90.0 .. 90.0 ]
// * Single-precision floating-point (float32) : longitude in decimal degrees, range [ -180.0 .. 180.0 ]
// * Single-precision floating-point (float32) : heading in decimal degrees, range [ 0.0 .. 360.0 [
#define OSEF_TYPE_GEOGRAPHIC_POSE 46UL

// Object ID 32 Bits
// **Output when**: Tracking is enabled.
// **Format**: Array of unsigned 32-bits integers. Each element of the array is an Object ID.
// **Purpose**: Give a unique identifier (ID) to each detected object.
// Optionally, this ID can also be used to establish a link between detected objects
// and the defined Zones, or the points from an Augmented Cloud.
// When used as leaf of Tracked Objects:
// - The number of elements of the array is equal to the Number of Objects (type 10).
// - Each ID represents a distinct detected object.
// - The affectation of an ID to an object is arbitrary and permanent,
//   it remains the same throughout the tracking phase.
// When used as leaf of Augmented Cloud:
// - The number of elements of the array is equal to the Number of Points (type 2).
// - Each specified ID indicates to which object the point belongs.
// - The special value 0 is used to mean 'no object'.
//   For instance, if the object ID of a point is 0,
//   it means that this point does not belong to any object.
// The Object ID is also used in Zones Objects Binding 32 Bits (type 48), combined with Zone identifier.
#define OSEF_TYPE_OBJECT_ID_32_BITS 47UL

// Zones Objects Binding 32 Bits
// **Output when**: Tracking is enabled, and at least one zone has been defined.
// **Purpose**: This type is used to know with tracked objects are in which event zones.
// The binary payload is the concatenation of 0 to N couples of:
// * an unsigned 32-bits integer, representing the ID of an object (see 47)
// * an unsigned 32-bits integer, representing the index of the event zone (the n-th type 34 of type 33)
// Each object-zone couple means that the object is considered by the algorithm to be in the zone.
#define OSEF_TYPE_ZONES_OBJECTS_BINDING_32_BITS 48UL

// Slam Pose Array
// **Output when**: Tracking/objects_super_resolution is enabled.
// **Purpose**: Pose of tracked objects. It is similar to type 29 "Pose Array", but it is more accurate since
// it uses a SLAM algorithm and not just a box fitting algorithm.
// The position part follows an arbitrary point on the object, which is not necessarily its center.
// The orientation part is arbitrary for the first predicted pose, then follows the rotation of the object.
// The SLAM algorithm may not run on all tracked objects. When the SLAM does not run on an object,
// let say on object number 5, all floats of the related pose in this array, e.g. the fifth, are equal to zero.
// Moreover, the related element of type 39 "Object properties", e.g. the fifth, has its third bit equal to zero.
// For further details see: [Spatial Data / Static
// Tracking](https://docs.outsight.ai/software/understanding-osef-data/spatial-data-tracking).
// **Format**: The binary payload is a repetition of the following content, one per tracked object:
// * single-precision floats Tx, Ty and Tz represent the translation vector T to get the position coordinates in
// meters.
// * single-precision floats Rxx, Rxy, Rxz, Ryx, Ryy, Ryz, Rzx, Rzy and Rzz represent the rotation matrix R defining
// the orientation.
// **Coordinate Frame**:
// Absolute/world, which is:
// - Single-LiDAR: On-Ground LiDAR Coordinate Frame.
// - Multi-LiDARs: Map Coordinate Frame.
#define OSEF_TYPE_SLAM_POSE_ARRAY 55UL

// Zone Vertical Limits
// **Output when**: Tracking is enabled, and at least a zone has been defined with Vertical Limits.
// **Format**: The binary value is the concatenation of:
// * one single-precision float, elevation, in meters
// * one single-precision float, height, in meters
// **Purpose**: Optional zone configuration which adds a filtering depending on the vertical position of objects.
// It is part of the definition of the zone.
// Only objects whose altitude is between elevation and elevation + height will be considered in the zone.
#define OSEF_TYPE_ZONE_VERTICAL_LIMITS 56UL

// Geographic Pose Precise
// **Output when**: Relocalization in a reference map and georeferencing are enabled.
// So in processing config: slam.enable set to true, slam.reference_map defined and georeferencing.enable set to true.
// **Purpose**: Represents the geographic pose, output from the relocalization processing, expressed in decimal
// degrees notation (latitude, longitude & heading).
// **Format**: Concatenation of 2 double precision (64 bits) and a single precision (32 bits) floats [lat, long,
// heading].
// * Double-precision floating-point (float64) : latitude in decimal degrees, range [ -90.0 .. 90.0 ]
// * Double-precision floating-point (float64) : longitude in decimal degrees, range [ -180.0 .. 180.0 ]
// * Single-precision floating-point (float32) : heading in decimal degrees, range [ 0.0 .. 360.0 [
#define OSEF_TYPE_GEOGRAPHIC_POSE_PRECISE 57UL

// Smoothed Pose
// **Output when**: SLAM is enabled.
// **Purpose**: Smoothed pose of the absolute LiDAR pose will introduce a slight delay.
// **Format**: Concatenation of 12 floats of 32-bits each, see type Pose for more details.
#define OSEF_TYPE_SMOOTHED_POSE 59UL

// Divergence Indicator
// **Since**: 5.6.0
// **Output when**: Ego-Motion or Relocalization is enabled.
// **Format**: One float value.
// **Purpose**: Indicates if SLAM seems to have diverged.
// - 0 means SLAM algorithm does not seem to have diverged.
// - 1 means SLAM algorithm seems to have diverged, the associated pose is not trustworthy.
#define OSEF_TYPE_DIVERGENCE_INDICATOR 71UL

// Geographic Pose Array
// **Output when**: Tracking is enabled and georeferencing is activated.
// **Purpose**: Represents the geographic pose, output from the relocalization processing, expressed in decimal
// degrees notation (latitude, longitude & heading) of each tracked object.
// **Format**: Concatenation of 2 double precision (64 bits) and a single precision (32 bits) floats [lat, long,
// heading], one for each tracked object.
// * Double-precision floating-point (float64): latitude in decimal degrees, range [ -90.0 .. 90.0 ]
// * Double-precision floating-point (float64): longitude in decimal degrees, range [ -180.0 .. 180.0 ]
// * Single-precision floating-point (float32): heading in decimal degrees, range [ 0.0 .. 360.0 [
#define OSEF_TYPE_GEOGRAPHIC_POSE_ARRAY 79UL

// Geographic Speed
// **Output when**: Georeferencing is activated.
// **Purpose**: Represents speed of mobile platform (on which LiDAR is mounted) as speed in m/s and geographic heading
// in decimal degrees.
// **Format**: Concatenation of two single-precision (32-bits) floats per object:
// * v: absolute speed in meters per second, range [ 0 .. +inf ]
// * heading: speed vector heading in decimal degrees, range [ 0.0 .. 360.0 [
#define OSEF_TYPE_GEOGRAPHIC_SPEED 80UL

// Geographic Speed Array
// **Output when**: Tracking is enabled and georeferencing is activated.
// **Purpose**: Represents speed of tracked objects represented as speed in m/s and geographic heading in decimal
// degrees.
// **Format**: Array of geographic speeds, two single-precision (32 bits) floats per object:
// * v: absolute speed in meters per second, range [ 0 .. +inf ]
// * heading: speed vector heading in decimal degrees, range [ 0.0 .. 360.0 [
#define OSEF_TYPE_GEOGRAPHIC_SPEED_ARRAY 81UL

// Reference Map Bits
// **Output when**: SLAM is enabled and its mapping module is enabled.
// **Purpose**: SLAM identifies potential points of interest within the point cloud, and the mapping module selects
// some of them to build the map. These bits identify the points that are selected to generate the output map.
// **Format**: Padded list of bits, 1 bit per point of the cloud. If the bit is set, the point is part of the
// reference map.
#define OSEF_TYPE_REFERENCE_MAP_BITS 86UL

// Coordinates Reference System
// **Output when**: Augmented Cloud is enabled.
// **Purpose**: The coordinates of the augmented cloud may be expressed in an absolute or relative coordinate system.
// The latter usually being relative to the the sensor. This field indicates the reference system in use.
// **Format**: Enumerate COORDINATES REFERENCE SYSTEM
#define OSEF_TYPE_COORDINATES_REFERENCE_SYSTEM 89UL

namespace Osef
{
// Tracked object class ID
enum class class_id_e : int32_t {
	UNKNOWN = 0,
	PERSON = 1,
	LUGGAGE = 2,
	TROLLEY = 3,
	DEPRECATED = 4,
	TRUCK = 5,
	CAR = 6,
	VAN = 7,
	TWO_WHEELER = 8,
	MASK = 9,
	NO_MASK = 10,
	LANDMARK = 11,
	TRAILER = 12,
	TRACTOR_HEAD = 13,
};

// Coordinates Reference System
enum class coordinates_reference_system_e : uint8_t {
	UNDEFINED = 0,
	WORLD = 1,
	SENSOR = 2,
};

}; // namespace Osef

// OSEF version: 1.14.0