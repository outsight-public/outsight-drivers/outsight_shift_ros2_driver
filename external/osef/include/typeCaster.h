#pragma once

#include <array>
#include <assert.h>
#include <cstdint>
#include <string.h>
#include <string>
#include <vector>

#include "tlvParser.h"

namespace TimestampMicroseconds
{
struct timestamp_s {
	uint32_t seconds = 0;
	uint32_t micro_seconds = 0;
};
static_assert(sizeof(timestamp_s) == 4 * 2, "timestamp_s size must be 8 bytes");

timestamp_s castValue(const Tlv::tlv_s *leafTlv);
} // namespace TimestampMicroseconds

namespace ZonesObjectsBinding
{
#pragma pack(push, 1)
struct binding_s {
	uint64_t obj_pid = 0;
	uint32_t zone_index = 0;
};
#pragma pack(pop)

std::vector<binding_s> castValue(const Tlv::tlv_s *leafTlv);
} // namespace ZonesObjectsBinding

namespace ZonesObjectsBinding32Bits
{
#pragma pack(push, 1)
struct binding_s {
	uint32_t obj_pid = 0;
	uint32_t zone_index = 0;
};
#pragma pack(pop)

std::vector<binding_s> castValue(const Tlv::tlv_s *leafTlv);
std::vector<binding_s> castValue(const std::vector<ZonesObjectsBinding::binding_s> *binding64b);
} // namespace ZonesObjectsBinding32Bits

namespace NumberOfObjects
{
uint32_t castValue(const Tlv::tlv_s *leafTlv);
}

namespace ObjectIds
{
std::vector<uint64_t> castValue(const Tlv::tlv_s *leafTlv);
}

namespace ObjectIds32Bits
{
std::vector<uint32_t> castValue(const Tlv::tlv_s *leafTlv);
std::vector<uint32_t> castValue(const std::vector<uint64_t> *object_ids_64b);
} // namespace ObjectIds32Bits

namespace Pose
{
const std::array<float, 3> castTranslationValue(const Tlv::tlv_s *leafTlv, size_t index = 0);
const std::array<float, 9> castRotationValue(const Tlv::tlv_s *leafTlv, size_t index = 0);
} // namespace Pose

namespace Cartesian
{
const std::array<float, 3> castValue(const Tlv::tlv_s *leafTlv, size_t pointIndex);
} // namespace Cartesian

// Template class to cast a TLV to a vector.
template <typename T>
std::vector<T> castValue(const Tlv::tlv_s *leafTlv)
{
	T *parsed = (T *)(leafTlv->getValue());
	size_t nb_of_elements(leafTlv->getLength() / sizeof(T));

	std::vector<T> data(nb_of_elements);
	for (size_t i = 0; i < nb_of_elements; ++i)
		data[i] = parsed[i];
	return data;
}

namespace ZoneVertices
{
struct zone_vertices_s {
	zone_vertices_s(size_t n = 0) : x(n), y(n)
	{
	}
	size_t size() const
	{
		return x.size();
	}
	std::vector<float> x = std::vector<float>{};
	std::vector<float> y = std::vector<float>{};
};
zone_vertices_s castValue(const Tlv::tlv_s *leafTlv);
} // namespace ZoneVertices
//
namespace ZoneVerticalLimits
{
struct zone_vertical_limits_s {
	float elevation = 0.f;
	float height = 0.f;
};
zone_vertical_limits_s castValue(const Tlv::tlv_s *leafTlv);
} // namespace ZoneVerticalLimits
