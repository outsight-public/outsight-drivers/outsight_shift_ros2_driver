#include "typeCaster.h"

TimestampMicroseconds::timestamp_s TimestampMicroseconds::castValue(const Tlv::tlv_s *leafTlv)
{
	assert(leafTlv->getLength() == sizeof(TimestampMicroseconds::timestamp_s));
	TimestampMicroseconds::timestamp_s *castedValue = (TimestampMicroseconds::timestamp_s *)(leafTlv->getValue());
	return *castedValue;
}

std::vector<ZonesObjectsBinding::binding_s> ZonesObjectsBinding::castValue(const Tlv::tlv_s *leafTlv)
{
	size_t nb_of_elements(leafTlv->getLength() / sizeof(ZonesObjectsBinding::binding_s));
	ZonesObjectsBinding::binding_s *parsed = (ZonesObjectsBinding::binding_s *)(leafTlv->getValue());
	std::vector<ZonesObjectsBinding::binding_s> bindings(nb_of_elements);
	for (size_t i = 0; i < nb_of_elements; ++i)
		bindings[i] = parsed[i];

	return bindings;
}

std::vector<ZonesObjectsBinding32Bits::binding_s> ZonesObjectsBinding32Bits::castValue(const Tlv::tlv_s *leafTlv)
{
	size_t nb_of_elements(leafTlv->getLength() / sizeof(ZonesObjectsBinding32Bits::binding_s));
	ZonesObjectsBinding32Bits::binding_s *parsed = (ZonesObjectsBinding32Bits::binding_s *)(leafTlv->getValue());
	std::vector<ZonesObjectsBinding32Bits::binding_s> bindings(nb_of_elements);
	for (size_t i = 0; i < nb_of_elements; ++i)
		bindings[i] = parsed[i];

	return bindings;
}

std::vector<ZonesObjectsBinding32Bits::binding_s>
ZonesObjectsBinding32Bits::castValue(const std::vector<ZonesObjectsBinding::binding_s> *binding64b)
{
	std::vector<ZonesObjectsBinding32Bits::binding_s> binding32(binding64b->size());
	for (size_t i = 0; i < binding64b->size(); ++i) {
		binding32[i].obj_pid = (uint32_t)binding64b->at(i).obj_pid;
		binding32[i].zone_index = binding64b->at(i).zone_index;
	}
	return binding32;
}

uint32_t NumberOfObjects::castValue(const Tlv::tlv_s *leafTlv)
{
	uint32_t count;
	memcpy(&count, leafTlv->getValue(),
	       sizeof(uint32_t)); // Only on little-endian arch
	return count;
}

std::vector<uint64_t> ObjectIds::castValue(const Tlv::tlv_s *leafTlv)
{
	uint64_t *parsed = (uint64_t *)(leafTlv->getValue());

	size_t nb_of_elements(leafTlv->getLength() / sizeof(uint64_t));

	std::vector<uint64_t> object_ids(nb_of_elements);
	for (size_t i = 0; i < nb_of_elements; ++i)
		object_ids[i] = parsed[i];
	return object_ids;
}

std::vector<uint32_t> ObjectIds32Bits::castValue(const Tlv::tlv_s *leafTlv)
{
	uint32_t *parsed = (uint32_t *)(leafTlv->getValue());

	size_t nb_of_elements(leafTlv->getLength() / sizeof(uint32_t));

	std::vector<uint32_t> object_ids(nb_of_elements);
	for (size_t i = 0; i < nb_of_elements; ++i)
		object_ids[i] = parsed[i];
	return object_ids;
}

std::vector<uint32_t> ObjectIds32Bits::castValue(const std::vector<uint64_t> *object_ids_64b)
{
	std::vector<uint32_t> object_ids_32b(object_ids_64b->begin(), object_ids_64b->end());
	return object_ids_32b;
}

const std::array<float, 3> Pose::castTranslationValue(const Tlv::tlv_s *leafTlv, size_t index)
{
	float *raw_pose = (float *)(leafTlv->getValue() + 12 * sizeof(float) * index);
	return std::array<float, 3>({ raw_pose[0], raw_pose[1], raw_pose[2] });
}

const std::array<float, 9> Pose::castRotationValue(const Tlv::tlv_s *leafTlv, size_t index)
{
	float *raw_pose = (float *)(leafTlv->getValue() + 12 * sizeof(float) * index);
	return std::array<float, 9>({ raw_pose[3], raw_pose[4], raw_pose[5], raw_pose[6], raw_pose[7], raw_pose[8],
				      raw_pose[9], raw_pose[10], raw_pose[11] });
}

const std::array<float, 3> Cartesian::castValue(const Tlv::tlv_s *leafTlv, size_t pointIndex)
{
	float *raw_value = (float *)(leafTlv->getValue() + 3 * sizeof(float) * pointIndex);
	return std::array<float, 3>({ raw_value[0], raw_value[1], raw_value[2] });
}

ZoneVertices::zone_vertices_s ZoneVertices::castValue(const Tlv::tlv_s *leafTlv)
{
	float *parsed = (float *)(leafTlv->getValue());

	size_t nb_of_elements(leafTlv->getLength() / sizeof(float) / 2);

	ZoneVertices::zone_vertices_s res(nb_of_elements);

	for (size_t i = 0; i < nb_of_elements; i++) {
		res.x[i] = parsed[2 * i];
		res.y[i] = parsed[2 * i + 1];
	}
	return res;
}

ZoneVerticalLimits::zone_vertical_limits_s ZoneVerticalLimits::castValue(const Tlv::tlv_s *leafTlv)
{
	float *parsed = (float *)(leafTlv->getValue());

	return ZoneVerticalLimits::zone_vertical_limits_s{ parsed[0], parsed[1] };
}
