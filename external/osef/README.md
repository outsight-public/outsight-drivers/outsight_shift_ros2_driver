# Osef library

## Scope

This library provides a parser to read an OSEF (Open SErialization Format) file or stream.

## Files

```
osef
├── data_types.md           # Description of OSEF data types
├── README.md               # This file
├── include
|   ├── osefTypes.h         # Definition of the outsight-specific types (T of TLV) used to serialize data
│   ├── tlvCommon.h         # General definition of TLV format
│   ├── tlvParser.h         # Header of TLV parsing
│   ├── tcpStream.h         # Header of TCP stream management (using socket) to read a live stream
│   └── typeCaster.h        # Header of Type casting from binary values to C++ types
└── src
    ├── tlvParser.cpp       # Implementation of TLV parsing
    ├── tcpStream.cpp       # Implementation of TCP stream management
    └── typeCaster.cpp      # Implementation of Type casting from binary values to C++ types.
```