#ifndef _ALB_PUBLISHER_H
#define _ALB_PUBLISHER_H

// ROS headers
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/nav_sat_fix.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include "sensor_msgs/msg/time_reference.hpp"

#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"
#include "tf2_ros/transform_listener.h"

// Osef headers
#include "tlvParser.h"

// Local header
#include "outsight_shift_driver/msg/augmented_cloud.hpp"
#include "outsight_shift_driver/msg/geo_ref_tracked_objects.hpp"
#include "outsight_shift_driver/msg/tracked_objects.hpp"
#include "outsight_shift_driver/msg/zone_bindings.hpp"
#include "outsight_shift_driver/msg/zones.hpp"

/// Class to publish ROS messages from the ALB.
class ShiftPublisher {
    public:
	ShiftPublisher(std::shared_ptr<rclcpp::Node> &node);

	void init();

	/// \brief Publish messages from the ALB frame.
	void publish(const Tlv::tlv_s *frame);

    private:
	void declareParameters();

	/// \brief Initialize the ROS publishers.
	void initPublishers();

	/// \brief Initialize the transform frames.
	void initTransforms();

	/// \brief Build ALB zones when the processing starts.
	void buildZones();

	/// \brief Publish TimeReference message.
	void publishTimeReference(const Tlv::tlv_s *frame);

	/// \brief Publish pointCloud message.
	void publishPointCloud(const Tlv::tlv_s *frame);

	/// \brief Publish pose message.
	void publishPose(const Tlv::tlv_s *frame);

	/// \brief Publish georeferenced tracked objects message.
	void publishGeoRefTrackedObjects(const Tlv::tlv_s *frame);

	/// \brief Publish tracked objects message.
	void publishTrackedObjects(const Tlv::tlv_s *frame);

	/// \brief Publish egomotion message.
	void publishEgomotion(const Tlv::tlv_s *frame);

	/// \brief Publish odometry message.
	void publishOdometry(const Tlv::tlv_s *frame);

	/// \brief Publish the augmented cloud message.
	void publishAugmentedCloud(const Tlv::tlv_s *frame);

	/// \brief Publish the zones message.
	void publishZones(const Tlv::tlv_s *frame);

	/// \brief Publish the zones_bindings message.
	void publishZoneBindings(const Tlv::tlv_s *frame);

	/// \brief Broadcast the ALB calculated GPS position.
	void publishGpsPose(const Tlv::tlv_s *frame);

	/// \brief Update current position as the last position.
	void updatePose(const Tlv::tlv_s *frame);

	/// \brief Update used time stamp with the ALB time stamp.
	void updateTimeStamp(const Tlv::tlv_s *frame);

    private:
	/// \brief Broadcast the ALB fixed frame to sensor frame transform.
	void broadcastShiftTransform(const geometry_msgs::msg::Pose &pose);

	/// \brief Broadcast the ALB map to fixed frame transform.
	void broadcastMapTransform(const geometry_msgs::msg::Pose &pose);

    private:
	std::shared_ptr<rclcpp::Node> node;
	std::shared_ptr<rclcpp::Clock> clock;

	std::string fixed_frame_id;
	std::string base_frame_id;
	std::string sensor_frame_id;
	std::string map_frame_id;

	geometry_msgs::msg::Pose last_pose;
	geometry_msgs::msg::Pose current_pose;
	geometry_msgs::msg::Pose first_pose;
	nav_msgs::msg::Odometry odom_msg;
	bool pose_found;
	bool get_first_pose = false;
	outsight_shift_driver::msg::Zones zones_msg;

	rclcpp::Time ros_time;
	rclcpp::Time shift_time;
	rclcpp::Time last_timestamp;
	rclcpp::Time current_timestamp;
	bool use_shift_time;

	bool use_colwise_order;

	std::shared_ptr<rclcpp::Publisher<sensor_msgs::msg::TimeReference>> time_reference_publisher;
	std::shared_ptr<rclcpp::Publisher<sensor_msgs::msg::PointCloud2>> point_cloud_publisher;
	std::shared_ptr<rclcpp::Publisher<geometry_msgs::msg::PoseStamped>> pose_publisher;
	std::shared_ptr<rclcpp::Publisher<outsight_shift_driver::msg::GeoRefTrackedObjects>>
		georef_tracked_objects_publisher;
	std::shared_ptr<rclcpp::Publisher<outsight_shift_driver::msg::TrackedObjects>> tracked_objects_publisher;
	std::shared_ptr<rclcpp::Publisher<geometry_msgs::msg::PoseStamped>> egomotion_publisher;
	std::shared_ptr<rclcpp::Publisher<nav_msgs::msg::Odometry>> odometry_publisher;
	std::shared_ptr<rclcpp::Publisher<outsight_shift_driver::msg::AugmentedCloud>> augmented_cloud_publisher;
	std::shared_ptr<rclcpp::Publisher<outsight_shift_driver::msg::Zones>> zones_publisher;
	std::shared_ptr<rclcpp::Publisher<outsight_shift_driver::msg::ZoneBindings>> zone_bindings_publisher;
	std::shared_ptr<rclcpp::Publisher<sensor_msgs::msg::NavSatFix>> gps_pose_publisher;

	tf2::Transform base_to_lidar_tf;
	bool transform_broadcaster;
	bool use_base_frame;
};

#endif // _ALB_PUBLISHER_H
