#ifndef _ALB_READER_H
#define _ALB_READER_H

// Standard headers
#include <memory>

// ROS headers
#include <rclcpp/rclcpp.hpp>

// Osef headers
#include "tcpStream.h"
#include "tlvParser.h"

// Local headers
#include "shift_publisher.h"

/// Class to read data from the ALB live stream.
class ShiftReader {
    public:
	ShiftReader(std::shared_ptr<rclcpp::Node> &node);

	/// \brief Initialize the parser.
	bool init();

	/// \brief Parse data from the input stream (ROS callback).
	/// \return
	/// - 0 on success.
	/// - -1 on error.
	int parse();

    private:
	/// \brief Check if the TLV is parsable.
	/// \return
	/// - True if parsing is possible.
	/// - False if not.
	bool isTlvParsable(const Tlv::tlv_s *frame);

	/// \brief Declare the ROS input arguments.
	/// \return
	/// - True if arguments are valid.
	/// - False in case of error.
	bool declareInputArguments();

	/// \brief Connect the parser to a live stream.
	/// \return
	/// - Error code.
	int connectToLiveStream();

    private:
	std::shared_ptr<rclcpp::Node> node;

	TcpStreamReader tcp_reader;
	std::string ip_v4;
	int port;

	ShiftPublisher shift_publisher;
	std::unique_ptr<uint8_t[]> buffer;
	bool shift_initialized;
	int disconnected_since_nb_frame = 0;
	int max_disconnected_since_nb_frame;
};

#endif // _ALB_READER_H
