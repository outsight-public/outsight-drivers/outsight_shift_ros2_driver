#ifndef _ALB_HELPER_H
#define _ALB_HELPER_H

// ROS headers
#include "geographic_msgs/msg/geo_pose.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "sensor_msgs/msg/nav_sat_fix.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"

// Osef headers
#include "tlvCommon.h"

// Local headers
#include "outsight_shift_driver/msg/augmented_cloud.hpp"
#include "outsight_shift_driver/msg/geo_ref_object_data.hpp"
#include "outsight_shift_driver/msg/object_data.hpp"
#include "outsight_shift_driver/msg/zone_bindings.hpp"
#include "outsight_shift_driver/msg/zones.hpp"

/// Namespace to define helper functions.
namespace Helper
{
/// \brief Define the point fields for the ALB PointCloud2 message.
void define_point_fields(sensor_msgs::msg::PointCloud2 &pointCloud);

/// \brief Define the PointCloud2 point data from raw points coming from the ALB.
void define_points_data(sensor_msgs::msg::PointCloud2 &pointCloud, const uint32_t layers, const uint32_t points,
			float *pointData, bool use_colwise_order);

/// \brief Define a ROS Pose message from ALB position and rotation.
void define_pose(geometry_msgs::msg::Pose &pose, const std::array<float, 3> &position,
		 const std::array<float, 9> &rotation);

/// \brief Initialize a ROS Pose message.
void init_pose(geometry_msgs::msg::Pose &pose);

/// \brief Parse the zone data in the raw string and set them in the Zones msg
void parse_zone_data(const std::string &raw_zones, outsight_shift_driver::msg::Zones &zones_msg);

/// \brief Get a ROS pose from the TLV data.
/// \return
/// - True if Pose is defined.
/// - False on error.
bool get_pose_from_tlv(const Tlv::tlv_s *frame, geometry_msgs::msg::PoseStamped &pose);

/// \brief Get a ROS egomotion pose from the TLV data.
/// \return
/// - True if Pose is defined.
/// - False on error.
bool get_egomotion_from_tlv(const Tlv::tlv_s *frame, geometry_msgs::msg::PoseStamped &pose);

/// \brief Get a GPS (NavSatFix) ROS msg from the TLV data. No heading information is published.
void get_gps_pose_from_tlv(const Tlv::tlv_s *frame, sensor_msgs::msg::NavSatFix &navSat_msg);

/// \brief Get a ROS Zones message from the TLV data.
void get_zones_from_tlv(const Tlv::tlv_s *frame, outsight_shift_driver::msg::Zones &zones_msg);

/// \brief Get a ROS ZoneBindings message from the TLV data.
void get_zone_bindings_from_tlv(const Tlv::tlv_s *frame, const outsight_shift_driver::msg::Zones &zones_msg,
				outsight_shift_driver::msg::ZoneBindings &zone_bindings_msg);

/// \brief Compute the odometry between the last two poses.
void computeOdometry(nav_msgs::msg::Odometry &odom, const geometry_msgs::msg::Pose &current_pose,
		     const geometry_msgs::msg::Pose &last_pose, const geometry_msgs::msg::Pose &first_pose, float dt);

/// \brief Get the string class of the tracked object from the ALB.
std::string get_object_class(uint32_t classId);

/// \brief Define the bounding box size for the tracked object.
void define_box_size(outsight_shift_driver::msg::ObjectData &tracked, const Tlv::tlv_s *bBoxSizes, size_t objectIndex);

/// \brief Define the bounding box pose for the tracked object.
void define_box_pose(outsight_shift_driver::msg::ObjectData &tracked, const Tlv::tlv_s *bBoxPoses, size_t objectIndex);

/// \brief Define the tracked object speed.
void define_object_speed(outsight_shift_driver::msg::ObjectData &tracked, const Tlv::tlv_s *objectSpeeds,
			 size_t objectIndex);

/// \brief Parse the bounding box size for the tracked object.
geometry_msgs::msg::Vector3 parse_box_size(const Tlv::tlv_s *bBoxSizes, size_t objectIndex);

/// \brief Parse the georeferenced pose for the tracked object.
geographic_msgs::msg::GeoPose parse_georef_pose(const Tlv::tlv_s *poses, size_t objectIndex);

/// \brief Parse the georeferenced tracked object ENU speed.
geometry_msgs::msg::Twist parse_georef_speed(const Tlv::tlv_s *objectSpeeds, size_t objectIndex);

/// \brief Define the AugmentedCloud message.
void define_augmented_cloud(outsight_shift_driver::msg::AugmentedCloud &message, const Tlv::tlv_s *frame);
}; // namespace Helper

#endif //_ALB_HELPER_H
