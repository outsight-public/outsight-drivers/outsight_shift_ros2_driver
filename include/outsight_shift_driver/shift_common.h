#ifndef _ALB_COMMON_H
#define _ALB_COMMON_H

/// Namespace to define common ALB values.
namespace ALBCommon
{
// Constant definition of the node frequencies
// In case of init failure, frequency at which to retry to init
constexpr double INIT_RETRY_FREQUENCY = 0.5;
// The main loop will block on parsing input OSEF frames.
// However, in case of failures, we need a fallback frequency
// to avoid saturating the CPU will connection requests
constexpr double MAIN_LOOP_FREQUENCY = 20;

// Constant definition of return codes.
constexpr int SC_MALFORMED_TLV = -1;
constexpr int SC_LONG_DISCONNECTION = -2;

// Constant definition of topics.
constexpr const char *k_shift_point_cloud = "shift/point_cloud";
constexpr const char *k_shift_pose = "shift/pose";
constexpr const char *k_shift_time_ref = "shift/time_reference";
constexpr const char *k_shift_objects = "shift/tracked_objects";
constexpr const char *k_shift_georef_objects = "shift/georef_tracked_objects";
constexpr const char *k_shift_egomotion = "shift/egomotion";
constexpr const char *k_shift_odometry = "shift/odometry";
constexpr const char *k_shift_augmented_cloud = "shift/augmented_cloud";
constexpr const char *k_shift_zones = "shift/zones";
constexpr const char *k_shift_zone_bindings = "shift/zone_bindings";
constexpr const char *k_shift_gps_pose = "shift/gps_pose";

// Constant definition of IP configuration of the ALB.
constexpr const char *k_shift_ip = "ip_address";
constexpr const char *k_shift_port = "ip_port";

constexpr const char *k_shift_max_disconnected_since_nb_frame = "shift_max_disconnected_since_nb_frame";

// Constant definition of frames.
constexpr const char *k_shift_fixed_frame_id = "frames.fixed_frame_id";
constexpr const char *k_shift_sensor_frame_id = "frames.sensor_frame_id";
constexpr const char *k_shift_base_frame_id = "frames.base_frame_id";
constexpr const char *k_shift_map_frame_id = "frames.map_frame_id";

// Constant definition of output parameters.
constexpr const char *k_shift_point_cloud_config = "output.point_cloud";
constexpr const char *k_shift_pose_config = "output.pose";
constexpr const char *k_shift_time_ref_config = "output.time_reference";
constexpr const char *k_shift_use_shift_time = "output.use_shift_time";
constexpr const char *k_shift_georef_objects_config = "output.georef_tracked_objects";
constexpr const char *k_shift_objects_config = "output.tracked_objects";
constexpr const char *k_shift_egomotion_config = "output.egomotion";
constexpr const char *k_shift_odometry_config = "output.odometry";
constexpr const char *k_shift_augmented_cloud_config = "output.augmented_cloud";
constexpr const char *k_shift_zones_config = "output.zones";
constexpr const char *k_parse_zones_from_osef = "output.parse_zones_from_osef";
constexpr const char *k_shift_zone_bindings_config = "output.zone_bindings";
constexpr const char *k_shift_broadcast_tf_config = "output.broadcast_tf";
constexpr const char *k_shift_use_colwise_order = "output.use_colwise_order";
constexpr const char *k_shift_gps_pose_config = "output.gps_pose";

// Constant definition of services.
constexpr const char *k_shift_processing_restart = "shift/processing/restart";
constexpr const char *k_shift_processing_stop = "shift/processing/stop";
constexpr const char *k_shift_processing_get_config = "shift/processing/get_config";
constexpr const char *k_shift_processing_put_config = "shift/processing/put_config";

constexpr const char *k_shift_storage_download = "shift/storage/download";
constexpr const char *k_shift_storage_upload = "shift/storage/upload";
constexpr const char *k_shift_storage_list = "shift/storage/list";

// Constant definition for zones config fields.
constexpr const char *k_zone_config_zones = "zones";
constexpr const char *k_zone_config_name = "name";
constexpr const char *k_zone_config_id = "id";
constexpr const char *k_zone_config_points = "points";
constexpr const char *k_zone_config_role = "role";
constexpr const char *k_zone_config_event = "event";

}; // namespace ALBCommon

#endif // _ALB_COMMON_H
