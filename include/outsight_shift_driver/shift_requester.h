#ifndef _ALB_REQUESTER_H
#define _ALB_REQUESTER_H

// ROS headers
#include "rclcpp/rclcpp.hpp"
#include "std_srvs/srv/trigger.hpp"

// Local headers
#include "outsight_shift_driver/srv/shift_config.hpp"
#include "outsight_shift_driver/srv/shift_file.hpp"

/// Class to send requests to the ALB.
class ShiftRequester {
    public:
	ShiftRequester(std::shared_ptr<rclcpp::Node> &node);

	/// \brief Initialize the ALB requester.
	/// \return
	/// - True if initialization is successful.
	/// - False on error.
	bool init(void);

    private:
	/// \brief Define the ALB services.
	void defineServices();

	/// \brief Service callback to start the processing.
	void restartProcessingCallback(std::shared_ptr<std_srvs::srv::Trigger::Request> request,
				       std::shared_ptr<std_srvs::srv::Trigger::Response> response);

	/// \brief Service callback to kill the processing.
	void stopProcessingCallback(std::shared_ptr<std_srvs::srv::Trigger::Request> request,
				    std::shared_ptr<std_srvs::srv::Trigger::Response> response);

	/// \brief Service callback to get the ALB configuration.
	void getConfigCallback(std::shared_ptr<outsight_shift_driver::srv::ShiftConfig::Request> request,
			       std::shared_ptr<outsight_shift_driver::srv::ShiftConfig::Response> response);

	/// \brief Service callback to put the ALB configuration.
	void putConfigCallback(std::shared_ptr<outsight_shift_driver::srv::ShiftConfig::Request> request,
			       std::shared_ptr<outsight_shift_driver::srv::ShiftConfig::Response> response);

	/// \brief Service callback to download a file from the ALB.
	void downloadFileCallback(std::shared_ptr<outsight_shift_driver::srv::ShiftFile::Request> request,
				  std::shared_ptr<outsight_shift_driver::srv::ShiftFile::Response> response);

	/// \brief Service callback to upload a file to the ALB.
	void uploadFileCallback(std::shared_ptr<outsight_shift_driver::srv::ShiftFile::Request> request,
				std::shared_ptr<outsight_shift_driver::srv::ShiftFile::Response> response);

	/// \brief Service callback to list files in the ALB.
	void listFilesCallback(std::shared_ptr<outsight_shift_driver::srv::ShiftFile::Request> request,
			       std::shared_ptr<outsight_shift_driver::srv::ShiftFile::Response> response);

    private:
	std::shared_ptr<rclcpp::Node> node;

	std::string ip_address;

	std::shared_ptr<rclcpp::Service<std_srvs::srv::Trigger>> processing_service_restart;
	std::shared_ptr<rclcpp::Service<std_srvs::srv::Trigger>> processing_service_stop;
	std::shared_ptr<rclcpp::Service<outsight_shift_driver::srv::ShiftConfig>> processing_service_get_config;
	std::shared_ptr<rclcpp::Service<outsight_shift_driver::srv::ShiftConfig>> processing_service_put_config;

	std::shared_ptr<rclcpp::Service<outsight_shift_driver::srv::ShiftFile>> storage_service_download;
	std::shared_ptr<rclcpp::Service<outsight_shift_driver::srv::ShiftFile>> storage_service_upload;
	std::shared_ptr<rclcpp::Service<outsight_shift_driver::srv::ShiftFile>> storage_service_list;
};

#endif // _ALB_REQUESTER_H
