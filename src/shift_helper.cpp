// Standard headers
#include "outsight_shift_driver/msg/detail/zone_data__struct.hpp"
#include <algorithm>
#include <cstddef>
#include <cstring>
#include <map>

// Json parser
#include <nlohmann/json.hpp>

// ROS headers
#include "geographic_msgs/msg/geo_pose.hpp"
#include "geometry_msgs/msg/point.hpp"
#include "rclcpp/rclcpp.hpp"
#include "tf2/LinearMath/Quaternion.h"
#include "tf2/LinearMath/Vector3.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"

// Osef headers
#include "osefTypes.h"
#include "typeCaster.h"

// Local headers
#include "shift_common.h"
#include "shift_helper.h"

using json = nlohmann::json;

// Constant definition.
const std::map<int, std::string> k_object_class = { { 0, "UNKNOWN" },  { 1, "PERSON" },	      { 2, "LUGGAGE" },
						    { 3, "TROLLEY" },  { 4, "TRUCK" },	      { 5, "BUS" },
						    { 6, "CAR" },      { 7, "VAN" },	      { 8, "TWO_WHEELER" },
						    { 9, "MASK" },     { 10, "NO_MASK" },     { 11, "LANDMARK" },
						    { 12, "TRAILER" }, { 13, "TRACTOR_HEAD" } };

constexpr const char *EVENT = "event";

namespace Helper
{
void define_point_fields(sensor_msgs::msg::PointCloud2 &pointCloud)
{
	sensor_msgs::msg::PointField x_field;
	x_field.name = "x";
	x_field.offset = 0;
	x_field.datatype = sensor_msgs::msg::PointField::FLOAT32;
	x_field.count = 1;

	sensor_msgs::msg::PointField y_field;
	y_field.name = "y";
	y_field.offset = 4;
	y_field.datatype = sensor_msgs::msg::PointField::FLOAT32;
	y_field.count = 1;

	sensor_msgs::msg::PointField z_field;
	z_field.name = "z";
	z_field.offset = 8;
	z_field.datatype = sensor_msgs::msg::PointField::FLOAT32;
	z_field.count = 1;

	pointCloud.fields.push_back(x_field);
	pointCloud.fields.push_back(y_field);
	pointCloud.fields.push_back(z_field);
}

void define_points_data(sensor_msgs::msg::PointCloud2 &pointCloud, const uint32_t layers, const uint32_t points,
			float *pointData, bool use_colwise_order)
{
	// Height and width define the number of points ("width" points in "height" dimension);
	if (!layers) {
		pointCloud.height = 1;
		pointCloud.width = points;
	} else {
		pointCloud.height = layers;
		pointCloud.width = points / layers;
	}

	// Steps define the data storage.
	constexpr size_t point_size = 3 * sizeof(float); // Size of a point in bytes
	pointCloud.point_step = point_size;
	pointCloud.row_step = pointCloud.width * pointCloud.point_step;

	// Point data have to be reinterpreted into uint8_t.
	const uint8_t *point_data = reinterpret_cast<uint8_t *>(pointData);
	const size_t total_size = point_size * points;
	if (use_colwise_order || !layers) {
		pointCloud.data.assign(point_data, point_data + total_size);
	} else {
		pointCloud.data.resize(total_size);
		size_t colwise_idx = 0;
		for (uint32_t w = 0; w < pointCloud.width; ++w) {
			for (uint32_t h = 0; h < layers; ++h, colwise_idx += point_size) {
				size_t rowwise_idx = (h * pointCloud.width + w) * point_size;
				std::copy(&point_data[colwise_idx], &point_data[colwise_idx] + point_size,
					  &(pointCloud.data[rowwise_idx]));
			}
		}
	}
	pointCloud.is_dense = true;
}

void define_pose(geometry_msgs::msg::Pose &pose, const std::array<float, 3> &position,
		 const std::array<float, 9> &rotation)
{
	pose.position.x = position[0];
	pose.position.y = position[1];
	pose.position.z = position[2];

	// Convert rotation matrix to quaternion orientation. (Stuelpnagel 1964)
	float t = rotation[0] + rotation[4] + rotation[8];

	if (1 + t > 0.0f) {
		float r = sqrt(1 + t);
		float s = 1 / (2 * r);

		pose.orientation.x = (rotation[5] - rotation[7]) * s;
		pose.orientation.y = (rotation[6] - rotation[2]) * s;
		pose.orientation.z = (rotation[1] - rotation[3]) * s;
		pose.orientation.w = r / 2;
	} else if (rotation[0] > rotation[4] && rotation[0] > rotation[8]) {
		float r = sqrt(1.0f + rotation[0] - rotation[4] - rotation[8]);
		float s = 1 / (2 * r);
		pose.orientation.x = r / 2;
		pose.orientation.y = (rotation[1] + rotation[3]) * s;
		pose.orientation.z = (rotation[2] + rotation[6]) * s;
		pose.orientation.w = (rotation[5] - rotation[7]) * s;
	} else if (rotation[4] > rotation[8]) {
		float r = sqrt(1.0f + rotation[4] - rotation[0] - rotation[8]);
		float s = 1 / (2 * r);
		pose.orientation.x = (rotation[1] + rotation[3]) * s;
		pose.orientation.y = r / 2;
		pose.orientation.z = (rotation[5] + rotation[7]) * s;
		pose.orientation.w = (rotation[6] - rotation[2]) * s;
	} else {
		float r = sqrt(1.0f + rotation[8] - rotation[0] - rotation[4]);
		float s = 1 / (2 * r);
		pose.orientation.x = (rotation[6] + rotation[2]) * s;
		pose.orientation.y = (rotation[5] + rotation[7]) * s;
		pose.orientation.z = r / 2;
		pose.orientation.w = (rotation[1] - rotation[3]) * s;
	}

	// Normalize the quaternion.
	tf2::Quaternion current_quat;
	tf2::fromMsg(pose.orientation, current_quat);
	current_quat.normalize();
	pose.orientation = tf2::toMsg(current_quat);
}

void init_pose(geometry_msgs::msg::Pose &pose)
{
	pose.position.x = 0.0f;
	pose.position.y = 0.0f;
	pose.position.z = 0.0f;
	pose.orientation.x = 0.0f;
	pose.orientation.y = 0.0f;
	pose.orientation.z = 0.0f;
	pose.orientation.w = 1.0f;
}

void parse_zone_data(const std::string &raw_zones, outsight_shift_driver::msg::Zones &zones_msg)
{
	const json parsed_zones = json::parse(raw_zones);

	for (auto zone_it = parsed_zones[ALBCommon::k_zone_config_zones].begin();
	     zone_it != parsed_zones[ALBCommon::k_zone_config_zones].end(); ++zone_it) {
		if ((*zone_it)[ALBCommon::k_zone_config_role].get<std::string>() == ALBCommon::k_zone_config_event) {
			outsight_shift_driver::msg::ZoneData zone;
			zone.name = (*zone_it)[ALBCommon::k_zone_config_name].get<std::string>();
			zone.id = (*zone_it)[ALBCommon::k_zone_config_id].get<std::string>();

			for (auto pt_it = (*zone_it)[ALBCommon::k_zone_config_points].begin();
			     pt_it != (*zone_it)[ALBCommon::k_zone_config_points].end(); pt_it++) {
				geometry_msgs::msg::Point point;
				point.x = (*pt_it)[0];
				point.y = (*pt_it)[1];
				zone.points.push_back(point);
			}

			zone.role = (*zone_it)[ALBCommon::k_zone_config_role].get<std::string>();
			zones_msg.zones.push_back(zone);
		}
	}
}

bool get_pose_from_tlv(const Tlv::tlv_s *frame, geometry_msgs::msg::PoseStamped &pose)
{
	auto logger = rclcpp::get_logger("shift_helper");

	Tlv::Parser parser(frame->getValue(), frame->getLength());
	const Tlv::tlv_s *frameTlv = parser.findTlv(OSEF_TYPE_SCAN_FRAME);
	if (!frameTlv) {
		RCLCPP_ERROR(logger, "[ShiftHelper] Scan frame not found, unable to publish PoseStamped message.");
		return false;
	}

	Tlv::Parser frameParser(frameTlv->getValue(), frameTlv->getLength());
	const Tlv::tlv_s *pose_tlv = frameParser.findTlv(OSEF_TYPE_POSE);
	if (!pose_tlv) {
		RCLCPP_ERROR(logger, "[ShiftHelper] Pose not found, unable to publish PoseStamped message.");
		return false;
	}

	std::array<float, 3> pose_translation(Pose::castTranslationValue(pose_tlv));
	std::array<float, 9> pose_rotation(Pose::castRotationValue(pose_tlv));

	define_pose(pose.pose, pose_translation, pose_rotation);

	return true;
}

bool get_egomotion_from_tlv(const Tlv::tlv_s *frame, geometry_msgs::msg::PoseStamped &pose)
{
	auto logger = rclcpp::get_logger("shift_helper");

	Tlv::Parser parser(frame->getValue(), frame->getLength());
	const Tlv::tlv_s *frameTlv = parser.findTlv(OSEF_TYPE_SCAN_FRAME);
	if (!frameTlv) {
		RCLCPP_ERROR(logger, "[ShiftHelper] Scan frame not found, unable to publish PoseStamped message.");
		return false;
	}

	Tlv::Parser frameParser(frameTlv->getValue(), frameTlv->getLength());
	const Tlv::tlv_s *egomotion_tlv = frameParser.findTlv(OSEF_TYPE_EGO_MOTION);
	if (!egomotion_tlv) {
		RCLCPP_ERROR(logger, "[ShiftHelper] Egomotion not found, unable to publish PoseStamped message.");
		return false;
	}

	Tlv::Parser egomotionParser(egomotion_tlv->getValue(), egomotion_tlv->getLength());
	const Tlv::tlv_s *pose_tlv = egomotionParser.findTlv(OSEF_TYPE_POSE_RELATIVE);
	if (!pose_tlv) {
		RCLCPP_ERROR(logger, "[ShiftHelper] Pose not found, unable to publish PoseStamped message.");
		return false;
	}

	std::array<float, 3> pose_translation(Pose::castTranslationValue(pose_tlv));
	std::array<float, 9> pose_rotation(Pose::castRotationValue(pose_tlv));

	define_pose(pose.pose, pose_translation, pose_rotation);

	return true;
}

void get_gps_pose_from_tlv(const Tlv::tlv_s *frame, sensor_msgs::msg::NavSatFix &navSat_msg)
{
	navSat_msg.latitude = ((double *)frame->getValue())[0];
	navSat_msg.longitude = ((double *)frame->getValue())[1];
}

void get_zones_from_tlv(const Tlv::tlv_s *frame, outsight_shift_driver::msg::Zones &zones_msg)
{
	// frame contains a list of ZoneDef TLVs
	Tlv::Parser zonesParser(frame->getValue(), frame->getLength());
	const Tlv::tlv_s *zone = zonesParser.nextTlv();

	std::vector<outsight_shift_driver::msg::ZoneData> zone_data_msg_vector;
	while (zone != nullptr) {
		outsight_shift_driver::msg::ZoneData zone_data_msg;

		Tlv::Parser zoneParser(zone->getValue(), zone->getLength());

		const Tlv::tlv_s *zone_vertices_tlv = zoneParser.findTlv(OSEF_TYPE_ZONE_VERTICES);
		ZoneVertices::zone_vertices_s zone_vertices = ZoneVertices::castValue(zone_vertices_tlv);

		const Tlv::tlv_s *zone_name_tlv = zoneParser.findTlv(OSEF_TYPE_ZONE_NAME);
		std::string zone_name((char *)zone_name_tlv->getValue());
		zone_data_msg.name = zone_name;

		const Tlv::tlv_s *zone_uuid_tlv = zoneParser.findTlv(37UL);
		if (zone_uuid_tlv != nullptr)
			std::string zone_uuid((char *)zone_uuid_tlv->getValue());

		const Tlv::tlv_s *zone_vertical_limits_tlv = zoneParser.findTlv(OSEF_TYPE_ZONE_VERTICAL_LIMITS);
		if (zone_uuid_tlv != nullptr) {
			ZoneVerticalLimits::zone_vertical_limits_s zone_vertical_limits =
				ZoneVerticalLimits::castValue(zone_vertical_limits_tlv);

			for (size_t vertex_index = 0; vertex_index < zone_vertices.size(); vertex_index++) {
				geometry_msgs::msg::Point pt1, pt2;
				pt1.x = zone_vertices.x[vertex_index];
				pt1.y = zone_vertices.y[vertex_index];
				pt1.z = zone_vertical_limits.elevation;
				pt2 = pt1;
				pt2.z = zone_vertical_limits.elevation + zone_vertical_limits.height;
				zone_data_msg.points.push_back(pt1);
				zone_data_msg.points.push_back(pt2);
			}
		} else {
			for (size_t vertex_index = 0; vertex_index < zone_vertices.size(); vertex_index++) {
				geometry_msgs::msg::Point pt;
				pt.x = zone_vertices.x[vertex_index];
				pt.y = zone_vertices.y[vertex_index];
				zone_data_msg.points.push_back(pt);
			}
		}

		// All zones streamed in the OSEF have the same role: Event
		zone_data_msg.role = EVENT;

		zone_data_msg_vector.push_back(zone_data_msg);
		zone = zonesParser.nextTlv();
	}
	zones_msg.zones = zone_data_msg_vector;
}

void get_zone_bindings_from_tlv(const Tlv::tlv_s *frame, const outsight_shift_driver::msg::Zones &zones_msg,
				outsight_shift_driver::msg::ZoneBindings &zone_bindings_msg)
{
	auto logger = rclcpp::get_logger("shift_helper");

	std::vector<ZonesObjectsBinding32Bits::binding_s> bindings = ZonesObjectsBinding32Bits::castValue(frame);
	zone_bindings_msg.zone_bindings.resize(bindings.size());

	for (size_t i(0); i < bindings.size(); i++) {
		const auto &binding = bindings[i];
		std::string zone_name = "";

		if (binding.zone_index < zones_msg.zones.size()) {
			zone_name = zones_msg.zones[binding.zone_index].name;
		} else {
			RCLCPP_ERROR(logger, "[ShiftHelper] ZoneBinding: zone index %d out of bound: %d zones.",
				     binding.zone_index, static_cast<int>(zones_msg.zones.size()));
		}

		zone_bindings_msg.zone_bindings[i].zone_name = zone_name;
		zone_bindings_msg.zone_bindings[i].zone_index = binding.zone_index;
		zone_bindings_msg.zone_bindings[i].object_id = binding.obj_pid;
	}
}

void computeOdometry(nav_msgs::msg::Odometry &odom, const geometry_msgs::msg::Pose &current_pose,
		     const geometry_msgs::msg::Pose &last_pose, const geometry_msgs::msg::Pose &first_pose, float dt)
{
	// Compute Pose displacement between first pose and current pose.
	geometry_msgs::msg::PoseStamped global_pose;
	geometry_msgs::msg::PoseStamped global_pose_from_first;
	global_pose.pose.position.x = current_pose.position.x - first_pose.position.x;
	global_pose.pose.position.y = current_pose.position.y - first_pose.position.y;
	global_pose.pose.position.z = current_pose.position.z - first_pose.position.z;

	// Define transform from first pose.
	geometry_msgs::msg::TransformStamped local_transform;
	local_transform.transform.rotation = first_pose.orientation;
	local_transform.transform.rotation.w = -local_transform.transform.rotation.w;

	tf2::doTransform(global_pose, global_pose_from_first, local_transform);

	// Odometry is the pose displacement.
	odom.pose.pose.position.x = global_pose_from_first.pose.position.x;
	odom.pose.pose.position.y = global_pose_from_first.pose.position.y;
	odom.pose.pose.position.z = global_pose_from_first.pose.position.z;

	// Compute rotation between first and current pose.
	tf2::Quaternion current_orientation;
	tf2::Quaternion first_orientation;
	tf2::Quaternion odom_orientation;
	tf2::fromMsg(current_pose.orientation, current_orientation);
	tf2::fromMsg(first_pose.orientation, first_orientation);
	odom_orientation = current_orientation * first_orientation.inverse();
	odom_orientation.normalize();
	odom.pose.pose.orientation = tf2::toMsg(odom_orientation);

	// Compute displacement between the current and last frame in the fixed frame
	geometry_msgs::msg::PoseStamped local_pose;
	geometry_msgs::msg::PoseStamped local_pose_from_first;
	local_pose.pose.position.x = current_pose.position.x - last_pose.position.x;
	local_pose.pose.position.y = current_pose.position.y - last_pose.position.y;
	local_pose.pose.position.z = current_pose.position.z - last_pose.position.z;

	// Compute the linear instant speed in the fixed frame
	tf2::doTransform(local_pose, local_pose_from_first, local_transform);

	odom.twist.twist.linear.x = local_pose_from_first.pose.position.x / dt;
	odom.twist.twist.linear.y = local_pose_from_first.pose.position.y / dt;
	odom.twist.twist.linear.z = local_pose_from_first.pose.position.z / dt;

	// Compute the angular instant speed
	tf2::Quaternion egomotion_orientation;
	tf2::Quaternion last_orientation;
	tf2::fromMsg(last_pose.orientation, last_orientation);
	egomotion_orientation = current_orientation * last_orientation.inverse();
	egomotion_orientation.normalize();
	tf2::Matrix3x3 m(egomotion_orientation);
	double roll = 0.0;
	double pitch = 0.0;
	double yaw = 0.0;
	m.getRPY(roll, pitch, yaw);

	odom.twist.twist.angular.x = roll / dt;
	odom.twist.twist.angular.y = pitch / dt;
	odom.twist.twist.angular.z = yaw / dt;
}

std::string get_object_class(uint32_t classId)
{
	std::string object_class("UNKNOWN");

	if (classId < k_object_class.size()) {
		object_class = k_object_class.at(classId);
	}

	return object_class;
}

void define_box_size(outsight_shift_driver::msg::ObjectData &tracked, const Tlv::tlv_s *bBoxSizes, size_t objectIndex)
{
	tracked.box_size = parse_box_size(bBoxSizes, objectIndex);
}

void define_box_pose(outsight_shift_driver::msg::ObjectData &tracked, const Tlv::tlv_s *bBoxPoses, size_t objectIndex)
{
	std::array<float, 3> pose_translation(Pose::castTranslationValue(bBoxPoses, objectIndex));
	std::array<float, 9> pose_rotation(Pose::castRotationValue(bBoxPoses, objectIndex));
	define_pose(tracked.pose, pose_translation, pose_rotation);
}

void define_object_speed(outsight_shift_driver::msg::ObjectData &tracked, const Tlv::tlv_s *objectSpeeds,
			 size_t objectIndex)
{
	float *speed_raw = (float *)(objectSpeeds->getValue() + 3 * sizeof(float) * objectIndex);

	tracked.speed.linear.x = speed_raw[0];
	tracked.speed.linear.y = speed_raw[1];
	tracked.speed.linear.z = speed_raw[2];
}

geometry_msgs::msg::Vector3 parse_box_size(const Tlv::tlv_s *bBoxSizes, size_t objectIndex)
{
	float *bbox_size_raw = (float *)(bBoxSizes->getValue() + 3 * sizeof(float) * objectIndex);
	geometry_msgs::msg::Vector3 res;

	res.x = bbox_size_raw[0];
	res.y = bbox_size_raw[1];
	res.z = bbox_size_raw[2];

	return res;
}

geographic_msgs::msg::GeoPose parse_georef_pose(const Tlv::tlv_s *poses, size_t objectIndex)
{
	constexpr std::size_t doubleSize = sizeof(double);
	constexpr std::size_t floatSize = sizeof(float);
	geographic_msgs::msg::GeoPose res;

	// Pointer arithmetic is based on the type of the data it points to
	// getValue() retrieves a uint8_t array, i.e. a byte array, so sizeof is used for indexation
	double *bufferPtr = (double *)(poses->getValue() + (2 * doubleSize + floatSize) * objectIndex);

	res.position.latitude = bufferPtr[0];
	res.position.longitude = bufferPtr[1];
	res.position.altitude = 0;

	// bufferPtr is a double*, so we add 2 to skip two doubles
	const float heading_degree = ((float *)(bufferPtr + 2))[0];
	const float heading_rad = heading_degree * M_PI / 180.f;

	// Shift use VY on the object to define the heading, and it id defined clockwise
	const float true_heading_rad = -(heading_rad - M_PI / 2.f);

	tf2::Quaternion quaternion(tf2::Vector3(0, 0, 1), true_heading_rad);
	res.orientation = tf2::toMsg(quaternion);

	return res;
}

geometry_msgs::msg::Twist parse_georef_speed(const Tlv::tlv_s *objectSpeeds, size_t objectIndex)
{
	float *speed_raw = (float *)(objectSpeeds->getValue() + 2 * sizeof(float) * objectIndex);
	geometry_msgs::msg::Twist res;

	float speed = speed_raw[0];
	float heading = speed_raw[1] * M_PI / 180.0;

	// Calculate ENU components
	double v_east = speed * sin(heading);
	double v_north = speed * cos(heading);

	res.linear.x = v_east;
	res.linear.y = v_north;
	res.linear.z = 0.f;

	res.angular.x = 0.f;
	res.angular.y = 0.f;
	res.angular.z = 0.f;

	return res;
}

void define_augmented_cloud(outsight_shift_driver::msg::AugmentedCloud &message, const Tlv::tlv_s *frame)
{
	Tlv::Parser aug_cloud_parser(frame->getValue(), frame->getLength());

	const Tlv::tlv_s *number_object_tlv = aug_cloud_parser.findTlv(OSEF_TYPE_NUMBER_OF_POINTS);
	const Tlv::tlv_s *number_layer_tlv = aug_cloud_parser.findTlv(OSEF_TYPE_NUMBER_OF_LAYERS);
	message.number_of_points = NumberOfObjects::castValue(number_object_tlv);
	message.number_of_layers = NumberOfObjects::castValue(number_layer_tlv);

	const Tlv::tlv_s *reflectivities_tlv = aug_cloud_parser.findTlv(OSEF_TYPE_REFLECTIVITIES);
	if (reflectivities_tlv) {
		message.reflectivities = std::vector<uint8_t>(
			reflectivities_tlv->getValue(), reflectivities_tlv->getValue() + message.number_of_points);
	}

	const Tlv::tlv_s *cartesian_tlv = aug_cloud_parser.findTlv(OSEF_TYPE_CARTESIAN_COORDINATES);
	if (cartesian_tlv) {
		float *cartesian = (float *)(cartesian_tlv->getValue());
		message.cartesian_coordinates = std::vector<float>(cartesian, cartesian + 3 * message.number_of_points);
	}

	const Tlv::tlv_s *object_ids_tlv = aug_cloud_parser.findTlv(OSEF_TYPE_OBJECT_ID_32_BITS);
	if (object_ids_tlv) {
		message.object_ids = ObjectIds32Bits::castValue(object_ids_tlv);
	}
}
} // namespace Helper
