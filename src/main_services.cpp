// ROS headers
#include "rclcpp/rclcpp.hpp"

// Local headers
#include "shift_requester.h"

int main(int argc, char **argv)
{
	rclcpp::init(argc, argv);
	auto node = rclcpp::Node::make_shared("shift_services");
	ShiftRequester requester(node);
	rclcpp::Rate rate(20);

	if (!requester.init()) {
		RCLCPP_ERROR(node->get_logger(), "[shift_services] Invalid initialization");
		return -1;
	}

	while (rclcpp::ok()) {
		rclcpp::spin_some(node);
		rate.sleep();
	}

	return 0;
}
