// ROS headers
#include "geometry_msgs/msg/point_stamped.hpp"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "outsight_shift_driver/msg/detail/geo_ref_tracked_objects__struct.hpp"
#include "rclcpp/time.hpp"
#include "sensor_msgs/msg/nav_sat_fix.hpp"
#include "sensor_msgs/msg/time_reference.hpp"

#include "tf2/convert.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"
#include "tf2_ros/buffer.h"
#include "tf2_ros/static_transform_broadcaster.h"
#include "tf2_ros/transform_broadcaster.h"
#include "tf2_ros/transform_listener.h"

// Osef headers
#include "osefTypes.h"
#include "typeCaster.h"

// Local headers
#include "shift_common.h"
#include "shift_curl_helper.h"
#include "shift_helper.h"
#include "shift_publisher.h"

#include "outsight_shift_driver/msg/augmented_cloud.hpp"
#include "outsight_shift_driver/msg/object_data.hpp"
#include "outsight_shift_driver/msg/tracked_objects.hpp"
#include "outsight_shift_driver/msg/zone_bindings.hpp"

// Constant definition.
constexpr const char *k_default_fixed_frame_id = "shift_fixed_frame";
constexpr const char *k_default_sensor_frame_id = "shift_sensor_frame";
constexpr const uint32_t k_queue_size = 1000;

ShiftPublisher::ShiftPublisher(std::shared_ptr<rclcpp::Node> &node)
	: node(node), clock(std::make_shared<rclcpp::Clock>())
{
	declareParameters();
}

void ShiftPublisher::declareParameters()
{
	node->declare_parameter(ALBCommon::k_shift_time_ref_config, false);
	node->declare_parameter(ALBCommon::k_shift_use_shift_time, false);
	node->declare_parameter(ALBCommon::k_shift_use_colwise_order, true);
	node->declare_parameter(ALBCommon::k_shift_pose_config, false);
	node->declare_parameter(ALBCommon::k_shift_point_cloud_config, false);
	node->declare_parameter(ALBCommon::k_shift_georef_objects_config, false);
	node->declare_parameter(ALBCommon::k_shift_objects_config, false);
	node->declare_parameter(ALBCommon::k_shift_odometry_config, false);
	node->declare_parameter(ALBCommon::k_shift_egomotion_config, false);
	node->declare_parameter(ALBCommon::k_shift_augmented_cloud_config, false);
	node->declare_parameter(ALBCommon::k_shift_zones_config, false);
	node->declare_parameter(ALBCommon::k_parse_zones_from_osef, true);
	node->declare_parameter(ALBCommon::k_shift_zone_bindings_config, false);
	node->declare_parameter(ALBCommon::k_shift_gps_pose_config, false);

	node->declare_parameter(ALBCommon::k_shift_fixed_frame_id, k_default_fixed_frame_id);
	node->declare_parameter(ALBCommon::k_shift_sensor_frame_id, k_default_sensor_frame_id);
	node->declare_parameter(ALBCommon::k_shift_base_frame_id, "");
	node->declare_parameter(ALBCommon::k_shift_map_frame_id, "");

	node->declare_parameter(ALBCommon::k_shift_broadcast_tf_config, false);
}

void ShiftPublisher::init()
{
	initPublishers();
	initTransforms();
	if (!node->get_parameter(ALBCommon::k_parse_zones_from_osef).get_value<bool>()) {
		buildZones();
	}

	Helper::init_pose(odom_msg.pose.pose);
	last_timestamp = clock->now();
}

void ShiftPublisher::initPublishers()
{
	// Define TimeReference publisher.
	if (node->get_parameter(ALBCommon::k_shift_time_ref_config).get_value<bool>()) {
		time_reference_publisher = node->create_publisher<sensor_msgs::msg::TimeReference>(
			ALBCommon::k_shift_time_ref, k_queue_size);
	}

	// Define used timestamp (ALB or ROS).
	node->get_parameter(ALBCommon::k_shift_use_shift_time, use_shift_time);

	// Define point cloud order
	node->get_parameter(ALBCommon::k_shift_use_colwise_order, use_colwise_order);

	// Define Pose publisher.
	if (node->get_parameter(ALBCommon::k_shift_pose_config).get_value<bool>()) {
		pose_publisher =
			node->create_publisher<geometry_msgs::msg::PoseStamped>(ALBCommon::k_shift_pose, k_queue_size);
	}

	// Define PointCloud publisher.
	if (node->get_parameter(ALBCommon::k_shift_point_cloud_config).get_value<bool>()) {
		point_cloud_publisher = node->create_publisher<sensor_msgs::msg::PointCloud2>(
			ALBCommon::k_shift_point_cloud, k_queue_size);
	}

	// Define GeoRefTrackedObjects publisher.
	if (node->get_parameter(ALBCommon::k_shift_georef_objects_config).get_value<bool>()) {
		georef_tracked_objects_publisher =
			node->create_publisher<outsight_shift_driver::msg::GeoRefTrackedObjects>(
				ALBCommon::k_shift_georef_objects, k_queue_size);
	}

	// Define TrackedObjects publisher.
	if (node->get_parameter(ALBCommon::k_shift_objects_config).get_value<bool>()) {
		tracked_objects_publisher = node->create_publisher<outsight_shift_driver::msg::TrackedObjects>(
			ALBCommon::k_shift_objects, k_queue_size);
	}

	// Define Odometry publisher.
	if (node->get_parameter(ALBCommon::k_shift_odometry_config).get_value<bool>()) {
		odometry_publisher =
			node->create_publisher<nav_msgs::msg::Odometry>(ALBCommon::k_shift_odometry, k_queue_size);
	}

	// Define Egomotion publisher.
	if (node->get_parameter(ALBCommon::k_shift_egomotion_config).get_value<bool>()) {
		egomotion_publisher = node->create_publisher<geometry_msgs::msg::PoseStamped>(
			ALBCommon::k_shift_egomotion, k_queue_size);
	}

	// Define AugmentedCloud publisher.
	if (node->get_parameter(ALBCommon::k_shift_augmented_cloud_config).get_value<bool>()) {
		augmented_cloud_publisher = node->create_publisher<outsight_shift_driver::msg::AugmentedCloud>(
			ALBCommon::k_shift_augmented_cloud, k_queue_size);
	}

	// Define Zones publisher.
	if (node->get_parameter(ALBCommon::k_shift_zones_config).get_value<bool>()) {
		zones_publisher = node->create_publisher<outsight_shift_driver::msg::Zones>(ALBCommon::k_shift_zones,
											    k_queue_size);
	}

	// Define ZoneBindings publisher.
	if (node->get_parameter(ALBCommon::k_shift_zone_bindings_config).get_value<bool>()) {
		zone_bindings_publisher = node->create_publisher<outsight_shift_driver::msg::ZoneBindings>(
			ALBCommon::k_shift_zone_bindings, k_queue_size);
	}

	// Define GPS publisher.
	if (node->get_parameter(ALBCommon::k_shift_gps_pose_config).get_value<bool>()) {
		gps_pose_publisher =
			node->create_publisher<sensor_msgs::msg::NavSatFix>(ALBCommon::k_shift_gps_pose, k_queue_size);
	}
}

void ShiftPublisher::initTransforms()
{
	// Define the frame ID.
	fixed_frame_id = node->get_parameter(ALBCommon::k_shift_fixed_frame_id).get_value<std::string>();
	sensor_frame_id = node->get_parameter(ALBCommon::k_shift_sensor_frame_id).get_value<std::string>();
	base_frame_id = node->get_parameter(ALBCommon::k_shift_base_frame_id).get_value<std::string>();
	map_frame_id = node->get_parameter(ALBCommon::k_shift_map_frame_id).get_value<std::string>();

	// Broadcast the transforms
	transform_broadcaster = node->get_parameter(ALBCommon::k_shift_broadcast_tf_config).get_value<bool>();

	tf2_ros::Buffer tfBuffer(clock);
	tf2_ros::TransformListener tfListener(tfBuffer);
	try {
		if (base_frame_id.empty()) {
			RCLCPP_WARN(node->get_logger(),
				    "[ShiftPublisher] No base_frame_id specified in the config file.");
			return;
		}
		geometry_msgs::msg::TransformStamped base_to_lidar = tfBuffer.lookupTransform(
			base_frame_id, sensor_frame_id, rclcpp::Time(0), rclcpp::Duration::from_seconds(0.5));
		tf2::fromMsg(base_to_lidar.transform, base_to_lidar_tf);
		use_base_frame = true;
		RCLCPP_INFO(node->get_logger(), "[ShiftPublisher] Base to lidar transform received.");
	} catch (tf2::TransformException &ex) {
		RCLCPP_WARN(
			node->get_logger(),
			"[ShiftPublisher] No base to lidar transform provided, package will not be able to relocate Lidar frame in the robot frame.");
		RCLCPP_WARN(node->get_logger(), "[ShiftPublisher] %s", ex.what());
		use_base_frame = false;
		return;
	}
}

void ShiftPublisher::buildZones()
{
	std::string ip_address;
	try {
		if (!node->get_parameter(ALBCommon::k_shift_ip, ip_address))
			ip_address = node->declare_parameter<std::string>(ALBCommon::k_shift_ip);
	} catch (const rclcpp::exceptions::UninitializedStaticallyTypedParameterException &) {
		RCLCPP_ERROR(node->get_logger(), "[ShiftPublisher] IP address not defined");
		return;
	}

	ShiftCurlHelper curl_helper(ip_address);

	std::string raw_zones;
	if (!curl_helper.executeProcessingZones(raw_zones)) {
		RCLCPP_INFO(node->get_logger(), "%s", curl_helper.getErrorMessage().c_str());
		return;
	}

	zones_msg.header.frame_id = map_frame_id;

	Helper::parse_zone_data(raw_zones, zones_msg);
	RCLCPP_INFO(node->get_logger(), "[ShiftPublisher] %ld zones detected.", zones_msg.zones.size());
}

void ShiftPublisher::publish(const Tlv::tlv_s *frame)
{
	ros_time = clock->now();
	updateTimeStamp(frame);
	if (use_shift_time) {
		current_timestamp = shift_time;
	} else {
		current_timestamp = ros_time;
	}

	if (pose_publisher || odometry_publisher || transform_broadcaster || egomotion_publisher) {
		updatePose(frame);
	}

	if (time_reference_publisher) {
		publishTimeReference(frame);
	}

	if (pose_publisher) {
		publishPose(frame);
	}

	if (point_cloud_publisher) {
		publishPointCloud(frame);
	}

	if (georef_tracked_objects_publisher) {
		publishGeoRefTrackedObjects(frame);
	}

	if (tracked_objects_publisher) {
		publishTrackedObjects(frame);
	}

	if (egomotion_publisher) {
		publishEgomotion(frame);
	}

	if (odometry_publisher) {
		publishOdometry(frame);
	}

	if (augmented_cloud_publisher) {
		publishAugmentedCloud(frame);
	}

	if (zones_publisher) {
		publishZones(frame);
	}

	if (zone_bindings_publisher) {
		publishZoneBindings(frame);
	}

	if (gps_pose_publisher) {
		publishGpsPose(frame);
	}

	if (transform_broadcaster) {
		broadcastShiftTransform(current_pose);
	}

	if (pose_found) {
		last_pose = current_pose;
	}
	last_timestamp = current_timestamp;
}

void ShiftPublisher::publishTimeReference(const Tlv::tlv_s *frame)
{
	Tlv::Parser parser(frame->getValue(), frame->getLength());
	const Tlv::tlv_s *timeTlv = parser.findTlv(OSEF_TYPE_TIMESTAMP_MICROSECOND);
	if (!timeTlv) {
		RCLCPP_ERROR(node->get_logger(),
			     "[ShiftPublisher] Timestamp not found, unable to publish TimeReference.");
		return;
	}

	sensor_msgs::msg::TimeReference time_message;
	time_message.header.stamp = current_timestamp;
	time_message.header.frame_id = fixed_frame_id;
	if (use_shift_time) {
		time_message.time_ref = ros_time;
		time_message.source = "shift_time";
	} else {
		time_message.time_ref = shift_time;
		time_message.source = "ros_time";
	}

	time_reference_publisher->publish(time_message);
}

void ShiftPublisher::publishPointCloud(const Tlv::tlv_s *frame)
{
	Tlv::Parser parser(frame->getValue(), frame->getLength());
	const Tlv::tlv_s *frameTlv = parser.findTlv(OSEF_TYPE_SCAN_FRAME);
	if (!frameTlv) {
		RCLCPP_ERROR(node->get_logger(),
			     "[ShiftPublisher] Scan frame not found, unable to publish PointCloud.");
		return;
	}

	Tlv::Parser frameParser(frameTlv->getValue(), frameTlv->getLength());
	const Tlv::tlv_s *augmented_point_cloud = frameParser.findTlv(OSEF_TYPE_AUGMENTED_CLOUD);
	if (!augmented_point_cloud) {
		RCLCPP_ERROR(node->get_logger(),
			     "[ShiftPublisher] Augmented point cloud not found, unable to publish PointCloud.");
		return;
	}

	// To compute the point Cloud, get the cartesian coordinates and apply the pose transformation.
	const Tlv::Parser cloudParser(augmented_point_cloud->getValue(), augmented_point_cloud->getLength());
	const Tlv::tlv_s *cartesian_tlv = cloudParser.findTlv(OSEF_TYPE_CARTESIAN_COORDINATES);
	const Tlv::tlv_s *number_points_tlv = cloudParser.findTlv(OSEF_TYPE_NUMBER_OF_POINTS);
	const Tlv::tlv_s *number_layers_tlv = cloudParser.findTlv(OSEF_TYPE_NUMBER_OF_LAYERS);

	if (!cartesian_tlv || !number_points_tlv || !number_layers_tlv) {
		std::string error_msg(!cartesian_tlv ? "Cartesian not found. " : "");
		error_msg.append(!number_points_tlv ? "Number of points not found." : "");
		error_msg.append(!number_layers_tlv ? "Number of layers not found." : "");

		RCLCPP_ERROR(node->get_logger(), "[ShiftPublisher] Unable to publish PointCloud: %s",
			     error_msg.c_str());
		return;
	}

	const uint32_t number_points = NumberOfObjects::castValue(number_points_tlv);
	const uint32_t number_layers = NumberOfObjects::castValue(number_layers_tlv);
	sensor_msgs::msg::PointCloud2 point_cloud_msg;
	point_cloud_msg.header.stamp = current_timestamp;
	point_cloud_msg.header.frame_id = sensor_frame_id;

	Helper::define_point_fields(point_cloud_msg);
	Helper::define_points_data(point_cloud_msg, number_layers, number_points, (float *)(cartesian_tlv->getValue()),
				   use_colwise_order);

	point_cloud_publisher->publish(point_cloud_msg);
}

void ShiftPublisher::publishPose(const Tlv::tlv_s *frame)
{
	if (!pose_found) {
		return;
	}

	geometry_msgs::msg::PoseStamped pose_msg;
	pose_msg.header.stamp = current_timestamp;
	pose_msg.header.frame_id = map_frame_id;
	pose_msg.pose = current_pose;
	pose_publisher->publish(pose_msg);
}

void ShiftPublisher::publishGeoRefTrackedObjects(const Tlv::tlv_s *frame)
{
	Tlv::Parser parser(frame->getValue(), frame->getLength());
	const Tlv::tlv_s *frameTlv = parser.findTlv(OSEF_TYPE_SCAN_FRAME);
	if (!frameTlv) {
		RCLCPP_ERROR(node->get_logger(),
			     "[ShiftPublisher] Scan frame not found, unable to publish Georeferenced tracked objects.");
		return;
	}

	Tlv::Parser frameParser(frameTlv->getValue(), frameTlv->getLength());
	const Tlv::tlv_s *objectsTlv = frameParser.findTlv(OSEF_TYPE_TRACKED_OBJECTS);
	if (!objectsTlv) {
		RCLCPP_ERROR(node->get_logger(),
			     "[ShiftPublisher] Georeferenced tracked objects not found, unable to publish.");
		return;
	}

	Tlv::Parser objectsParser(objectsTlv->getValue(), objectsTlv->getLength());
	const Tlv::tlv_s *num_objects = objectsParser.findTlv(OSEF_TYPE_NUMBER_OF_OBJECTS);
	const Tlv::tlv_s *object_ids = objectsParser.findTlv(OSEF_TYPE_OBJECT_ID_32_BITS);
	const Tlv::tlv_s *bbox_sizes = objectsParser.findTlv(OSEF_TYPE_BBOX_SIZES);
	const Tlv::tlv_s *pose_array = objectsParser.findTlv(OSEF_TYPE_POSE_ARRAY);
	const Tlv::tlv_s *georef_pose_array = objectsParser.findTlv(OSEF_TYPE_GEOGRAPHIC_POSE_ARRAY);
	const Tlv::tlv_s *georef_speed_vectors = objectsParser.findTlv(OSEF_TYPE_GEOGRAPHIC_SPEED_ARRAY);
	const Tlv::tlv_s *class_ids = objectsParser.findTlv(OSEF_TYPE_CLASS_ID_ARRAY);
	if (!num_objects || !object_ids || !bbox_sizes || !pose_array || !georef_speed_vectors || !class_ids) {
		std::string error_msg(!num_objects ? "Number of objects not found. " : "");
		error_msg.append(!object_ids ? "Object ids not found. " : "");
		error_msg.append(!bbox_sizes ? "Box sizes not found. " : "");
		error_msg.append(
			!pose_array ?
				"Geographic pose array not found. Make sure shift tracked_objects georeferencing is enabled." :
				"");
		error_msg.append(
			!georef_speed_vectors ?
				"Geographic speed array not found. Make sure shift tracked_objects georeferencing is enabled." :
				"");
		error_msg.append(!class_ids ? "Class IDs not found. " : "");

		RCLCPP_ERROR(node->get_logger(), "[ShiftPublisher] Unable to publish GeoRefTrackedObjects message: %s.",
			     error_msg.c_str());
		return;
	}

	outsight_shift_driver::msg::GeoRefTrackedObjects tracked_message;
	tracked_message.header.stamp = current_timestamp;
	tracked_message.header.frame_id = fixed_frame_id;
	tracked_message.number_of_objects = NumberOfObjects::castValue(num_objects);
	uint32_t *ids = (uint32_t *)(object_ids->getValue());

	for (size_t box_index = 0; box_index < tracked_message.number_of_objects; box_index++) {
		outsight_shift_driver::msg::GeoRefObjectData object;

		object.id = ids[box_index];
		object.pose = Helper::parse_georef_pose(georef_pose_array, box_index);
		object.speed = Helper::parse_georef_speed(georef_speed_vectors, box_index);
		object.box_size = Helper::parse_box_size(bbox_sizes, box_index);

		uint32_t *class_id = (uint32_t *)(class_ids->getValue() + sizeof(uint32_t) * box_index);
		object.object_class = Helper::get_object_class(class_id[0]);

		tracked_message.objects.push_back(object);
	}

	georef_tracked_objects_publisher->publish(tracked_message);
}

void ShiftPublisher::publishTrackedObjects(const Tlv::tlv_s *frame)
{
	Tlv::Parser parser(frame->getValue(), frame->getLength());
	const Tlv::tlv_s *frameTlv = parser.findTlv(OSEF_TYPE_SCAN_FRAME);
	if (!frameTlv) {
		RCLCPP_ERROR(node->get_logger(),
			     "[ShiftPublisher] Scan frame not found, unable to publish TrackedObjects message.");
		return;
	}

	Tlv::Parser frameParser(frameTlv->getValue(), frameTlv->getLength());
	const Tlv::tlv_s *objectsTlv = frameParser.findTlv(OSEF_TYPE_TRACKED_OBJECTS);
	if (!objectsTlv) {
		RCLCPP_ERROR(node->get_logger(),
			     "[ShiftPublisher] Tracked objects not found, unable to publish TrackedObjects message.");
		return;
	}

	Tlv::Parser objectsParser(objectsTlv->getValue(), objectsTlv->getLength());
	const Tlv::tlv_s *num_objects = objectsParser.findTlv(OSEF_TYPE_NUMBER_OF_OBJECTS);
	const Tlv::tlv_s *object_ids = objectsParser.findTlv(OSEF_TYPE_OBJECT_ID_32_BITS);
	const Tlv::tlv_s *bbox_sizes = objectsParser.findTlv(OSEF_TYPE_BBOX_SIZES);
	const Tlv::tlv_s *pose_array = objectsParser.findTlv(OSEF_TYPE_POSE_ARRAY);
	const Tlv::tlv_s *speed_vectors = objectsParser.findTlv(OSEF_TYPE_SPEED_VECTORS);
	const Tlv::tlv_s *class_ids = objectsParser.findTlv(OSEF_TYPE_CLASS_ID_ARRAY);
	if (!num_objects || !object_ids || !bbox_sizes || !pose_array || !speed_vectors || !class_ids) {
		std::string error_msg(!num_objects ? "Number of objects not found. " : "");
		error_msg.append(!object_ids ? "Object ids not found. " : "");
		error_msg.append(!bbox_sizes ? "Box sizes not found. " : "");
		error_msg.append(!pose_array ? "Pose array not found. " : "");
		error_msg.append(!speed_vectors ? "Speed vectors not found. " : "");
		error_msg.append(!class_ids ? "Class IDs not found. " : "");

		RCLCPP_ERROR(node->get_logger(), "[ShiftPublisher] Unable to publish TrackedObjects message: %s.",
			     error_msg.c_str());
		return;
	}

	outsight_shift_driver::msg::TrackedObjects tracked_message;
	tracked_message.header.stamp = current_timestamp;
	tracked_message.header.frame_id = fixed_frame_id;
	tracked_message.number_of_objects = NumberOfObjects::castValue(num_objects);
	uint32_t *ids = (uint32_t *)(object_ids->getValue());

	for (size_t box_index = 0; box_index < tracked_message.number_of_objects; box_index++) {
		outsight_shift_driver::msg::ObjectData object;

		object.id = ids[box_index];
		Helper::define_box_size(object, bbox_sizes, box_index);
		Helper::define_box_pose(object, pose_array, box_index);
		Helper::define_object_speed(object, speed_vectors, box_index);

		uint32_t *class_id = (uint32_t *)(class_ids->getValue() + sizeof(uint32_t) * box_index);
		object.object_class = Helper::get_object_class(class_id[0]);

		tracked_message.objects.push_back(object);
	}

	tracked_objects_publisher->publish(tracked_message);
}

void ShiftPublisher::publishEgomotion(const Tlv::tlv_s *frame)
{
	if (!pose_found) {
		return;
	}

	geometry_msgs::msg::PoseStamped ego_msg;
	ego_msg.header.stamp = current_timestamp;
	ego_msg.header.frame_id = sensor_frame_id;
	if (!Helper::get_egomotion_from_tlv(frame, ego_msg)) {
		return;
	}

	egomotion_publisher->publish(ego_msg);
}

void ShiftPublisher::publishOdometry(const Tlv::tlv_s *frame)
{
	if (!pose_found) {
		return;
	}

	odom_msg.header.stamp = current_timestamp;
	odom_msg.header.frame_id = fixed_frame_id;
	odom_msg.child_frame_id = sensor_frame_id;

	float dt = (current_timestamp - last_timestamp).seconds();

	Helper::computeOdometry(odom_msg, current_pose, last_pose, first_pose, dt);

	odometry_publisher->publish(odom_msg);
}

void ShiftPublisher::publishAugmentedCloud(const Tlv::tlv_s *frame)
{
	Tlv::Parser parser(frame->getValue(), frame->getLength());
	const Tlv::tlv_s *frameTlv = parser.findTlv(OSEF_TYPE_SCAN_FRAME);
	if (!frameTlv) {
		RCLCPP_ERROR(node->get_logger(),
			     "[ShiftPublisher] Scan frame not found, unable to publish PointCloud.");
		return;
	}

	Tlv::Parser frameParser(frameTlv->getValue(), frameTlv->getLength());
	const Tlv::tlv_s *augmented_point_cloud = frameParser.findTlv(OSEF_TYPE_AUGMENTED_CLOUD);
	if (!augmented_point_cloud) {
		RCLCPP_ERROR(node->get_logger(),
			     "[ShiftPublisher] Augmented point cloud not found, unable to publish PointCloud.");
		return;
	}

	outsight_shift_driver::msg::AugmentedCloud message;
	message.header.stamp = current_timestamp;
	message.header.frame_id = sensor_frame_id;
	Helper::define_augmented_cloud(message, augmented_point_cloud);
	augmented_cloud_publisher->publish(message);
}

void ShiftPublisher::publishZones(const Tlv::tlv_s *frame)
{
	zones_msg.header.stamp = current_timestamp;

	// If we parse the zone from the OSEF: update the message each iteration
	if (node->get_parameter(ALBCommon::k_parse_zones_from_osef).get_value<bool>()) {
		Tlv::Parser parser(frame->getValue(), frame->getLength());
		const Tlv::tlv_s *frameTlv = parser.findTlv(OSEF_TYPE_SCAN_FRAME);
		if (!frameTlv) {
			RCLCPP_ERROR(node->get_logger(),
				     "[ShiftPublisher] Scan frame not found, unable to publish Zones.");
			return;
		}

		Tlv::Parser frameParser(frameTlv->getValue(), frameTlv->getLength());
		const Tlv::tlv_s *zones = frameParser.findTlv(OSEF_TYPE_ZONES_DEF);
		if (!zones) {
			RCLCPP_ERROR(node->get_logger(), "[ShiftPublisher] Zones not found, unable to publish.");
			return;
		}

		Helper::get_zones_from_tlv(zones, zones_msg);
	}

	zones_publisher->publish(zones_msg);
}

void ShiftPublisher::publishZoneBindings(const Tlv::tlv_s *frame)
{
	Tlv::Parser parser(frame->getValue(), frame->getLength());
	const Tlv::tlv_s *frameTlv = parser.findTlv(OSEF_TYPE_SCAN_FRAME);
	if (!frameTlv) {
		RCLCPP_ERROR(node->get_logger(),
			     "[ShiftPublisher] Scan frame not found, unable to publish ZoneBindings.");
		return;
	}

	Tlv::Parser frameParser(frameTlv->getValue(), frameTlv->getLength());
	const Tlv::tlv_s *zone_bindings = frameParser.findTlv(OSEF_TYPE_ZONES_OBJECTS_BINDING_32_BITS);
	if (!zone_bindings) {
		RCLCPP_ERROR(node->get_logger(), "[ShiftPublisher] ZoneBindings not found, unable to publish.");
		return;
	}

	outsight_shift_driver::msg::ZoneBindings message;
	message.header.stamp = current_timestamp;
	message.header.frame_id = sensor_frame_id;

	Helper::get_zone_bindings_from_tlv(zone_bindings, zones_msg, message);

	zone_bindings_publisher->publish(message);
}

void ShiftPublisher::publishGpsPose(const Tlv::tlv_s *frame)
{
	Tlv::Parser parser(frame->getValue(), frame->getLength());
	const Tlv::tlv_s *frameTlv = parser.findTlv(OSEF_TYPE_SCAN_FRAME);
	if (!frameTlv) {
		RCLCPP_ERROR(node->get_logger(), "[ShiftPublisher] Scan frame not found, unable to publish GPS pose.");
		return;
	}

	Tlv::Parser frameParser(frameTlv->getValue(), frameTlv->getLength());
	const Tlv::tlv_s *gps_pose_precise = frameParser.findTlv(OSEF_TYPE_GEOGRAPHIC_POSE_PRECISE);
	if (!gps_pose_precise) {
		RCLCPP_ERROR(node->get_logger(),
			     "[ShiftPublisher] Geographic pose precise not found, unable to publish GPS pose.");
		return;
	}

	sensor_msgs::msg::NavSatFix message;
	message.header.stamp = current_timestamp;
	message.header.frame_id = sensor_frame_id;

	Helper::get_gps_pose_from_tlv(gps_pose_precise, message);

	gps_pose_publisher->publish(message);
}

void ShiftPublisher::updatePose(const Tlv::tlv_s *frame)
{
	geometry_msgs::msg::PoseStamped pose_msg;
	pose_found = Helper::get_pose_from_tlv(frame, pose_msg);
	if (!pose_found) {
		return;
	}

	if (!get_first_pose) {
		broadcastMapTransform(pose_msg.pose);

		first_pose = pose_msg.pose;
		last_pose = pose_msg.pose;
		get_first_pose = true;
	}
	current_pose = pose_msg.pose;
}

void ShiftPublisher::updateTimeStamp(const Tlv::tlv_s *frame)
{
	Tlv::Parser parser(frame->getValue(), frame->getLength());
	const Tlv::tlv_s *timeTlv = parser.findTlv(OSEF_TYPE_TIMESTAMP_MICROSECOND);
	TimestampMicroseconds::timestamp_s timestamp_osef = TimestampMicroseconds::castValue(timeTlv);
	shift_time = rclcpp::Time(timestamp_osef.seconds, timestamp_osef.micro_seconds * 1000);
}

void ShiftPublisher::broadcastShiftTransform(const geometry_msgs::msg::Pose &pose)
{
	if (!odometry_publisher) {
		RCLCPP_WARN(
			node->get_logger(),
			"[ShiftPublisher] Transform between fixed frame and sensor frame requires odometry to be published. Set the odometry parameter to 'true' in order to broadcast this transform.");
		return;
	}

	static tf2_ros::TransformBroadcaster broadcaster(node);

	geometry_msgs::msg::Transform odom_to_lidar;
	odom_to_lidar.translation.x = odom_msg.pose.pose.position.x;
	odom_to_lidar.translation.y = odom_msg.pose.pose.position.y;
	odom_to_lidar.translation.z = odom_msg.pose.pose.position.z;
	odom_to_lidar.rotation = odom_msg.pose.pose.orientation;

	tf2::Transform odom_to_lidar_tf;
	tf2::fromMsg(odom_to_lidar, odom_to_lidar_tf);

	geometry_msgs::msg::TransformStamped transformStamped;
	transformStamped.header.stamp = current_timestamp;
	if (use_base_frame) {
		tf2::Transform odom_to_base_tf = odom_to_lidar_tf * base_to_lidar_tf.inverse();
		transformStamped.header.frame_id = fixed_frame_id;
		transformStamped.child_frame_id = base_frame_id;
		transformStamped.transform = tf2::toMsg(odom_to_base_tf);
	} else {
		transformStamped.header.frame_id = fixed_frame_id;
		transformStamped.child_frame_id = sensor_frame_id;
		transformStamped.transform = tf2::toMsg(odom_to_lidar_tf);
	}

	broadcaster.sendTransform(transformStamped);
}

void ShiftPublisher::broadcastMapTransform(const geometry_msgs::msg::Pose &pose)
{
	if (map_frame_id.empty()) {
		RCLCPP_WARN(node->get_logger(), "[ShiftPublisher] No map_frame_id specified in the config file");
	}
	static tf2_ros::StaticTransformBroadcaster static_broadcaster(node);
	geometry_msgs::msg::TransformStamped static_transformStamped;

	static_transformStamped.header.stamp = current_timestamp;
	static_transformStamped.header.frame_id = map_frame_id;
	static_transformStamped.child_frame_id = fixed_frame_id;
	static_transformStamped.transform.translation.x = pose.position.x;
	static_transformStamped.transform.translation.y = pose.position.y;
	static_transformStamped.transform.translation.z = pose.position.z;
	static_transformStamped.transform.rotation = pose.orientation;

	static_broadcaster.sendTransform(static_transformStamped);
}
