// ROS headers
#include "rclcpp/rclcpp.hpp"

// Osef headers
#include "osefTypes.h"

// Local headers
#include "shift_common.h"
#include "shift_reader.h"

// Constant definition.
constexpr const size_t k_buffer_size = 100000000;

namespace
{
// Check for Little/Big endian architecture.
// Returns true if BigEndian.
bool isBigEndianArchitecture()
{
	const uint32_t i = 0x01020304;
	return reinterpret_cast<const uint8_t *>(&i)[0] == 1;
}
}; // namespace

ShiftReader::ShiftReader(std::shared_ptr<rclcpp::Node> &node) : node(node), shift_publisher(node)
{
	shift_publisher.init();
}

bool ShiftReader::init()
{
	if (!declareInputArguments()) {
		return false;
	}

	if (connectToLiveStream() != 1) {
		RCLCPP_ERROR(node->get_logger(), "[ShiftReader] Unable to connect to live stream with address %s.",
			     ip_v4.c_str());
		return false;
	}

	// Initialize buffer for reading data.
	buffer = std::make_unique<uint8_t[]>(k_buffer_size);
	shift_initialized = true;

	return true;
}

int ShiftReader::parse()
{
	int ret = 0;
	ret = tcp_reader.getNextFrame(buffer.get(), k_buffer_size);

	if (ret != 1) {
		if (connectToLiveStream() != 1)
			disconnected_since_nb_frame += 1;
		if (disconnected_since_nb_frame > max_disconnected_since_nb_frame)
			return ALBCommon::SC_LONG_DISCONNECTION;
	} else {
		disconnected_since_nb_frame = 0;
		Tlv::tlv_s *frame = (Tlv::tlv_s *)buffer.get();

		if (!isTlvParsable(frame)) {
			return ALBCommon::SC_MALFORMED_TLV;
		}

		shift_publisher.publish(frame);
	}

	return ret;
}

bool ShiftReader::isTlvParsable(const Tlv::tlv_s *frame)
{
	// Check expected root TLV type.
	if (frame->getType() != OSEF_TYPE_TIMESTAMPED_DATA) {
		RCLCPP_ERROR(node->get_logger(), "[ShiftReader] Error: wrong frame TLV type");
		return false;
	}

	// Value parsing on big endian architecture is not supported by this code.
	if (isBigEndianArchitecture()) {
		RCLCPP_ERROR(node->get_logger(),
			     "[ShiftReader] Value parsing on big endian architecture is not supported");
		return false;
	}

	return true;
}

bool ShiftReader::declareInputArguments()
{
	try {
		if (!node->get_parameter(ALBCommon::k_shift_ip, ip_v4))
			ip_v4 = node->declare_parameter<std::string>(ALBCommon::k_shift_ip);
		if (!node->get_parameter(ALBCommon::k_shift_port, port))
			port = node->declare_parameter<int>(ALBCommon::k_shift_port);
		if (!node->get_parameter(ALBCommon::k_shift_max_disconnected_since_nb_frame,
					 max_disconnected_since_nb_frame)) {
			max_disconnected_since_nb_frame =
				node->declare_parameter<int>(ALBCommon::k_shift_max_disconnected_since_nb_frame);
		}
	} catch (const rclcpp::exceptions::ParameterAlreadyDeclaredException &) {
		RCLCPP_ERROR(node->get_logger(), "[ShiftReader] IP and port are not defined in parameters.");
		return false;
	}
	return true;
}

int ShiftReader::connectToLiveStream()
{
	if (ip_v4.empty()) {
		RCLCPP_ERROR(node->get_logger(), "[ShiftReader] IP of the ALB not defined.");
		return -1;
	}

	int ret = 0;
	tcp_reader.disconnectfromALB();
	ret = tcp_reader.connectToALB(ip_v4.c_str(), port);

	if (ret == 0) {
		RCLCPP_WARN(node->get_logger(),
			    "[ShiftReader] ALB not available yet (processing not started?) actively waiting "
			    "for it...");
		while ((ret = tcp_reader.connectToALB(ip_v4.c_str(), port)) == 0)
			;
	}

	if (shift_initialized) {
		RCLCPP_WARN(node->get_logger(), "[ShiftReader] Processing restarts with the ALB already initialized. "
						"Make sure that a reinitialization is not needed.");
	}

	return ret;
}
