// Standard headers
#include <map>
#include <stdio.h>

// ROS headers
#include "rclcpp/rclcpp.hpp"

// Local headers
#include "shift_curl_helper.h"

// Constant definition.
constexpr const char *k_http = "http://";
constexpr const char *k_api = "/api/v1/";
const std::map<ShiftCurlHelper::processing_service_e, std::string> k_processing_commands = {
	{ ShiftCurlHelper::processing_service_e::Restart, "processing/restart" },
	{ ShiftCurlHelper::processing_service_e::Stop, "processing/stop" },
	{ ShiftCurlHelper::processing_service_e::Status, "processing/status" }
};
constexpr const char *k_processing_config = "processing/config";
constexpr const char *k_processing_zones = "processing/zones";
constexpr const char *k_storage = "storage/";

constexpr const char *k_header_accept = "accept: ";
constexpr const char *k_header_content = "Content-Type: ";
constexpr const char *k_header_app_json = "application/json";
constexpr const char *k_header_app_octet = "application/octet-stream";

constexpr const char *k_display_config = "Display current ALB config";

// Server response error code.
constexpr const int k_server_success = 200;
constexpr const int k_server_upload_success = 201;

namespace
{
// Function to read the curl output.
static size_t write_callback(void *contents, size_t size, size_t nmemb, void *userp)
{
	((std::string *)userp)->append((char *)contents, size * nmemb);
	return size * nmemb;
}

// Function to write the curl output to a file.
static size_t write_to_file_callback(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
}
}; // namespace

ShiftCurlHelper::ShiftCurlHelper(const std::string &ip_address) : curl(nullptr), chunk(NULL), file(nullptr)
{
	// Define curl address.
	if (!ip_address.empty()) {
		curl_address = k_http + ip_address + k_api;
	}
}

ShiftCurlHelper::~ShiftCurlHelper(void)
{
	// Clean up curl.
	curl_easy_cleanup(curl);
	curl_slist_free_all(chunk);

	if (file) {
		fclose(file);
	}
}

const std::string ShiftCurlHelper::getErrorMessage(void) const
{
	return error_message;
}

bool ShiftCurlHelper::executeProcessing(const processing_service_e service)
{
	if (!initCurl()) {
		error_message = std::string("[ShiftCurlHelper] Invalid init");
		return false;
	}

	defineBaseCommand(k_processing_commands.at(service));

	// Define headers.
	chunk = curl_slist_append(chunk, std::string(k_header_accept).append(k_header_app_json).c_str());
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

	curl_easy_setopt(curl, CURLOPT_POST, 1L);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");

	return executeCurlCommand(k_processing_commands.at(service));
}

bool ShiftCurlHelper::isProcessingRunning(bool &running)
{
	if (!initCurl()) {
		error_message = std::string("[ShiftCurlHelper] Invalid init.");
		return false;
	}

	defineBaseCommand(k_processing_commands.at(ShiftCurlHelper::processing_service_e::Status));

	// Define headers.
	chunk = curl_slist_append(chunk, std::string(k_header_accept).append(k_header_app_json).c_str());
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

	if (!executeCurlCommand(k_processing_commands.at(ShiftCurlHelper::processing_service_e::Status))) {
		error_message = std::string("[ShiftCurlHelper] Unable to get processing status.");
		return false;
	}

	running = (callback_data.find("true") != std::string::npos);

	return true;
}

bool ShiftCurlHelper::executeProcessingConfig(const processing_config_e &service,
					      outsight_shift_driver::srv::ShiftConfig::Request &request)
{
	if (!initCurl()) {
		error_message = std::string("[ShiftCurlHelper] Invalid init");
		return false;
	}

	defineBaseCommand(k_processing_config);

	if (!defineProcessingConfig(service, request)) {
		return false;
	}

	if (!executeCurlCommand(k_processing_config)) {
		return false;
	}

	if (service == processing_config_e::Get) {
		if (!exportData(callback_data, request.filepath, k_display_config)) {
			error_message = std::string("[ShiftCurlHelper] Error exporting the config data.");
			return false;
		}
	}

	return true;
}

bool ShiftCurlHelper::executeProcessingZones(std::string &zones)
{
	if (!initCurl()) {
		error_message = std::string("[ShiftCurlHelper] Invalid init");
		return false;
	}

	defineBaseCommand(k_processing_zones);

	// Define Processing Zones for curl request
	chunk = curl_slist_append(chunk, std::string(k_header_accept).append(k_header_app_json).c_str());
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

	if (!executeCurlCommand(k_processing_zones)) {
		return false;
	}

	zones = callback_data;
	return true;
}

bool ShiftCurlHelper::executeStorage(const storage_service_e service,
				     outsight_shift_driver::srv::ShiftFile::Request &request)
{
	if (!initCurl()) {
		error_message = std::string("[ShiftCurlHelper] Invalid init");
		return false;
	}

	bool define_command = false;
	if (service == storage_service_e::List) {
		define_command = defineStorageListCommand(request);
	} else {
		define_command = defineStorageCommand(service, request);
	}

	if (!define_command) {
		return false;
	}

	if (!executeCurlCommand(k_storage)) {
		return false;
	}

	if (service == storage_service_e::List) {
		exportData(callback_data, request.filepath,
			   std::string("List ") + request.category + std::string(" files on ALB:"));
	}

	return true;
}

bool ShiftCurlHelper::initCurl(void)
{
	if (curl_address.empty()) {
		return false;
	}

	curl = curl_easy_init();
	return curl;
}

void ShiftCurlHelper::defineBaseCommand(const std::string &shift_command)
{
	std::string full_address;
	full_address = curl_address + shift_command;
	callback_data.clear();

	// Define the URL for the command.
	curl_easy_setopt(curl, CURLOPT_URL, full_address.c_str());

	// Define the callback function.
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &callback_data);
}

bool ShiftCurlHelper::defineProcessingConfig(const processing_config_e &service,
					     outsight_shift_driver::srv::ShiftConfig::Request &request)
{
	if (service == processing_config_e::Get) {
		chunk = curl_slist_append(chunk, std::string(k_header_accept).append(k_header_app_json).c_str());
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
	} else if (service == processing_config_e::Put) {
		chunk = curl_slist_append(chunk, std::string(k_header_accept).append(k_header_app_json).c_str());
		chunk = curl_slist_append(chunk, std::string(k_header_content).append(k_header_app_json).c_str());
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

		std::string post_data = readData(request.filepath);

		if (post_data.empty()) {
			error_message = std::string("No data for configuration.");
			return false;
		}

		// Manually set the PUT command because CURLOPT_POSTFIELDS defines the POST one.
		curl_easy_setopt(curl, CURLOPT_COPYPOSTFIELDS, post_data.c_str());
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
	}

	return true;
}

bool ShiftCurlHelper::defineStorageCommand(const storage_service_e service,
					   outsight_shift_driver::srv::ShiftFile::Request &request)
{
	std::string storage_command;
	storage_command.append(k_storage);
	storage_command.append(request.category);
	storage_command.append("/");
	storage_command.append(request.shift_filename);

	std::string full_address;
	full_address = curl_address + storage_command;
	callback_data.clear();

	// Define the URL for the command.
	curl_easy_setopt(curl, CURLOPT_URL, full_address.c_str());

	// Define the callback function.
	if (service == storage_service_e::Download) {
		file = fopen(request.filepath.c_str(), "wb");

		if (!file) {
			error_message = std::string("Unable to open the output filepath.");
			return false;
		}

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_file_callback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
	} else {
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &callback_data);

		chunk = curl_slist_append(chunk, std::string(k_header_accept).append(k_header_app_json).c_str());
		chunk = curl_slist_append(chunk, std::string(k_header_content).append(k_header_app_octet).c_str());
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

		file = fopen(request.filepath.c_str(), "rb");

		if (!file) {
			error_message = std::string("Unable to open the output filepath.");
			return false;
		}

		curl_easy_setopt(curl, CURLOPT_READDATA, file);
		curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
	}

	return true;
}

bool ShiftCurlHelper::executeCurlCommand(const std::string &command)
{
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 1L); // Set total timeout to 1 seconds
	CURLcode curl_code;
	curl_code = curl_easy_perform(curl);

	if (curl_code != CURLE_OK) {
		error_message = "curl command failed: ";
		error_message.append(curl_easy_strerror(curl_code));
		return false;
	}

	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &server_response);

	if ((server_response != k_server_success) && (server_response != k_server_upload_success)) {
		error_message = "Server error ";
		error_message.append(std::to_string(server_response));
		error_message.append("\nResponse body: ");
		error_message.append(callback_data);
		return false;
	}

	return true;
}

bool ShiftCurlHelper::exportData(const std::string &data, const std::string &output_path, const std::string &print_text)
{
	if (output_path.empty()) {
		RCLCPP_INFO(rclcpp::get_logger("shift_curl_helper"), "%s\n%s", print_text.c_str(), data.c_str());
		return true;
	}

	FILE *fd;
	fd = fopen(output_path.c_str(), "w");

	if (!fd) {
		return false;
	}

	fwrite(data.c_str(), sizeof(char), strlen(data.c_str()), fd);
	fclose(fd);

	return true;
}

const std::string ShiftCurlHelper::readData(const std::string &filepath)
{
	std::string data;
	FILE *fd;
	fd = fopen(filepath.c_str(), "r");

	if (!fd) {
		error_message = std::string("Unable to open file ") + filepath;
		return data;
	}

	fseek(fd, 0, SEEK_END);
	long fsize = ftell(fd);
	fseek(fd, 0, SEEK_SET);

	char *buffer;
	buffer = (char *)malloc(sizeof(char) * (fsize + 1));

	fread(buffer, 1, fsize, fd);
	buffer[fsize] = 0;
	fclose(fd);
	data = std::string(buffer);
	free(buffer);

	return data;
}

bool ShiftCurlHelper::defineStorageListCommand(const outsight_shift_driver::srv::ShiftFile::Request &request)
{
	std::string storage_command;
	storage_command.append(k_storage);
	storage_command.append(request.category);

	std::string full_address;
	full_address = curl_address + storage_command;
	callback_data.clear();

	// Define the URL for the command.
	curl_easy_setopt(curl, CURLOPT_URL, full_address.c_str());

	// Define the callback function.
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &callback_data);

	chunk = curl_slist_append(chunk, std::string(k_header_accept).append(k_header_app_json).c_str());
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

	return true;
}
