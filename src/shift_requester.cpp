// Local headers
#include "shift_requester.h"
#include "shift_common.h"
#include "shift_curl_helper.h"

ShiftRequester::ShiftRequester(std::shared_ptr<rclcpp::Node> &node) : node(node)
{
	defineServices();
}

bool ShiftRequester::init(void)
{
	// Define the curl address.
	try {
		if (!node->get_parameter(ALBCommon::k_shift_ip, ip_address))
			ip_address = node->declare_parameter<std::string>(ALBCommon::k_shift_ip);
	} catch (const rclcpp::exceptions::UninitializedStaticallyTypedParameterException &) {
		RCLCPP_ERROR(node->get_logger(), "[ShiftRequester] IP address not defined");
		return false;
	}

	return true;
}

void ShiftRequester::defineServices()
{
	using namespace std;
	using namespace std::placeholders;

	// Processing services.
	processing_service_restart = node->create_service<std_srvs::srv::Trigger>(
		ALBCommon::k_shift_processing_restart, bind(&ShiftRequester::restartProcessingCallback, this, _1, _2));
	processing_service_stop = node->create_service<std_srvs::srv::Trigger>(
		ALBCommon::k_shift_processing_stop, bind(&ShiftRequester::stopProcessingCallback, this, _1, _2));
	processing_service_get_config = node->create_service<outsight_shift_driver::srv::ShiftConfig>(
		ALBCommon::k_shift_processing_get_config, bind(&ShiftRequester::getConfigCallback, this, _1, _2));
	processing_service_put_config = node->create_service<outsight_shift_driver::srv::ShiftConfig>(
		ALBCommon::k_shift_processing_put_config, bind(&ShiftRequester::putConfigCallback, this, _1, _2));

	// Storage services.
	storage_service_download = node->create_service<outsight_shift_driver::srv::ShiftFile>(
		ALBCommon::k_shift_storage_download, bind(&ShiftRequester::downloadFileCallback, this, _1, _2));
	storage_service_upload = node->create_service<outsight_shift_driver::srv::ShiftFile>(
		ALBCommon::k_shift_storage_upload, bind(&ShiftRequester::uploadFileCallback, this, _1, _2));
	storage_service_list = node->create_service<outsight_shift_driver::srv::ShiftFile>(
		ALBCommon::k_shift_storage_list, bind(&ShiftRequester::listFilesCallback, this, _1, _2));
}

void ShiftRequester::restartProcessingCallback(std::shared_ptr<std_srvs::srv::Trigger::Request>,
					       std::shared_ptr<std_srvs::srv::Trigger::Response> response)
{
	ShiftCurlHelper helper(ip_address);

	if (!helper.executeProcessing(ShiftCurlHelper::processing_service_e::Restart)) {
		response->success = false;
		response->message = helper.getErrorMessage();
		return;
	}

	ShiftCurlHelper helperRun(ip_address);
	bool running = false;
	if (!helperRun.isProcessingRunning(running)) {
		response->success = false;
		response->message = helperRun.getErrorMessage();
		return;
	}

	if (running) {
		response->success = true;
		response->message = std::string("Processing successfully restarted on ALB.");
	} else {
		response->success = false;
		response->message = std::string("Unable to restart processing on ALB.");
	}
}

void ShiftRequester::stopProcessingCallback(std::shared_ptr<std_srvs::srv::Trigger::Request>,
					    std::shared_ptr<std_srvs::srv::Trigger::Response> response)
{
	ShiftCurlHelper helper(ip_address);

	if (!helper.executeProcessing(ShiftCurlHelper::processing_service_e::Stop)) {
		response->success = false;
		response->message = helper.getErrorMessage();
		return;
	}

	ShiftCurlHelper helperRun(ip_address);
	bool running = false;
	if (!helperRun.isProcessingRunning(running)) {
		response->success = false;
		response->message = helperRun.getErrorMessage();
		return;
	}

	if (!running) {
		response->success = true;
		response->message = std::string("Processing successfully stopped on ALB.");
	} else {
		response->success = false;
		response->message = std::string("Unable to stop processing on ALB.");
	}
}

void ShiftRequester::getConfigCallback(std::shared_ptr<outsight_shift_driver::srv::ShiftConfig::Request> request,
				       std::shared_ptr<outsight_shift_driver::srv::ShiftConfig::Response> response)
{
	ShiftCurlHelper helper(ip_address);

	if (!helper.executeProcessingConfig(ShiftCurlHelper::processing_config_e::Get, *request)) {
		response->success = false;
		response->message = helper.getErrorMessage();
		return;
	}

	response->success = true;
	response->message = std::string("Successfully get the config.");
}

void ShiftRequester::putConfigCallback(std::shared_ptr<outsight_shift_driver::srv::ShiftConfig::Request> request,
				       std::shared_ptr<outsight_shift_driver::srv::ShiftConfig::Response> response)
{
	ShiftCurlHelper helper(ip_address);

	if (!helper.executeProcessingConfig(ShiftCurlHelper::processing_config_e::Put, *request)) {
		response->success = false;
		response->message = helper.getErrorMessage();
		return;
	}

	response->success = true;
	response->message = std::string("Successfully put the config.");
}

void ShiftRequester::downloadFileCallback(std::shared_ptr<outsight_shift_driver::srv::ShiftFile::Request> request,
					  std::shared_ptr<outsight_shift_driver::srv::ShiftFile::Response> response)
{
	ShiftCurlHelper helper(ip_address);

	if (!helper.executeStorage(ShiftCurlHelper::storage_service_e::Download, *request)) {
		response->success = false;
		response->message = helper.getErrorMessage();
		return;
	}

	response->success = true;
	response->message = std::string("File successfully downloaded.");
}

void ShiftRequester::uploadFileCallback(std::shared_ptr<outsight_shift_driver::srv::ShiftFile::Request> request,
					std::shared_ptr<outsight_shift_driver::srv::ShiftFile::Response> response)
{
	ShiftCurlHelper helper(ip_address);

	if (!helper.executeStorage(ShiftCurlHelper::storage_service_e::Upload, *request)) {
		response->success = false;
		response->message = helper.getErrorMessage();
		return;
	}

	response->success = true;
	response->message = std::string("File successfully uploaded.");
}

void ShiftRequester::listFilesCallback(std::shared_ptr<outsight_shift_driver::srv::ShiftFile::Request> request,
				       std::shared_ptr<outsight_shift_driver::srv::ShiftFile::Response> response)
{
	ShiftCurlHelper helper(ip_address);

	if (!helper.executeStorage(ShiftCurlHelper::storage_service_e::List, *request)) {
		response->success = false;
		response->message = helper.getErrorMessage();
		return;
	}

	response->success = true;
	response->message = std::string("Files successfully displayed.");
}
