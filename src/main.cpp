#include <exception>

// ROS headers
#include "rclcpp/rclcpp.hpp"
#include <rclcpp/utilities.hpp>

// Local headers
#include "shift_common.h"
#include "shift_reader.h"

int main(int argc, char **argv)
{
	rclcpp::init(argc, argv);
	while (rclcpp::ok()) {
		auto node = rclcpp::Node::make_shared("shift_data");

		RCLCPP_INFO(node->get_logger(), "[shift_data] - Init node");
		ShiftReader reader(node);

		rclcpp::Rate init_try_rate(ALBCommon::INIT_RETRY_FREQUENCY);
		bool initialized = false;

		while (!initialized && rclcpp::ok()) {
			try {
				initialized = reader.init();
			} catch (std::exception &exp) {
				RCLCPP_ERROR(node->get_logger(), "[shift_data] Caught exception: %s", exp.what());
			}
			if (!initialized) {
				RCLCPP_ERROR(node->get_logger(),
					     "[shift_data] Invalid initialization. Retrying every two seconds.");
				init_try_rate.sleep();
			}
		}
		RCLCPP_INFO(node->get_logger(), "[shift_data] Init done");

		rclcpp::Rate main_loop_rate(ALBCommon::MAIN_LOOP_FREQUENCY);
		int parse_sc = 0;

		while (rclcpp::ok()) {
			try {
				parse_sc = reader.parse();
			} catch (std::exception &exp) {
				RCLCPP_ERROR(node->get_logger(), "[shift_data] Caught exception: %s", exp.what());
			}

			if (parse_sc == ALBCommon::SC_LONG_DISCONNECTION) {
				RCLCPP_ERROR(
					node->get_logger(),
					"[shift_data] Long deconnection. The node will be destroyed and reinstantiated.");
				break;
			}
			rclcpp::spin_some(node);
			main_loop_rate.sleep();
		}
	}
	return 0;
}
