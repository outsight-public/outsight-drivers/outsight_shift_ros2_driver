# outsight_shift_ros2_driver

ROS2 package for Outsight Shift lidar platform.

**Author**: [Outsight](https://www.outsight.ai/)

## Installation
### Prerequisite

Humble and Iron are officially supported.

* ROS2 ([Install ROS2 Humble](https://docs.ros.org/en/humble/Installation.html))
* ROS2 ([Install ROS2 Iron](https://docs.ros.org/en/iron/Installation.html))

### Building from sources
1. Clone the repository
2. Build using the ROS2 colcon build system

```sh
# Clone the package
mkdir -b outsight_shift_ros2_driver/src
git clone https://gitlab.com/outsight-public/outsight-drivers/outsight_shift_ros2_driver.git outsight_shift_ros2_driver/src

# Install dependencies
sudo apt update
rosdep update
rosdep install --from-paths src --ignore-src -r -y

# Build the package
colcon build
source ./install/setup.bash
```

### Configuration

* Configure Shift according to your application [Documentation](https://docs.outsight.ai/).
* `ip_address` and `ip_port` have to be defined in the launch file (See `launch/outsight_shift.launch` for example).
* To be able to publish messages to the corresponding topics and frames, set up the `config/shift_tcp.yaml` file.

  To manage the frames, several frame names are defined in the configuration file:
    
    * `fixed_frame_id` defined with default value at `shift_fixed_frame` represents the first frame when the processing starts. It is the fixed reference frame.
    * `sensor_frame_id` represents the sensor frame (linked with sensor), with default value at `shift_sensor_frame`.
    * `base_frame_id` represents the robot base frame (this frame is optional, see the provided transformation section for more information)
    * `map_frame_id` is the fixed frame of the world and allows to link the odometry origin to the map (this frame is also optional).

## Usage

Run the two built nodes, `shift_data` and `shift_services`.

```sh
ros2 launch outsight_shift_driver outsight_shift.launch
```

To manage multiple configurations, the config file (placed in the dedicated /config folder) can be given as an argument to the launch file.
```sh
ros2 launch outsight_shift_driver outsight_shift.launch config_file:=custom_config.yaml
```

Recommendation: if Shift processing is manually resumed, restart ROS processing as well to account for any changes
(for example if the processed record has been modified).

## Nodes
### shift_data

This node handles the received data from the ALB and publishes it to ROS.

#### Published topics
* `/shift/point_cloud` -> [sensor_msgs/msg/PointCloud2](https://docs.ros2.org/latest/api/sensor_msgs/msg/PointCloud2.html)
* `/shift/pose` -> [geometry_msgs/msg/PoseStamped](https://docs.ros2.org/latest/api/geometry_msgs/msg/PoseStamped.html)
* `/shift/egomotion` -> [geometry_msgs/msg/PoseStamped](https://docs.ros2.org/latest/api/geometry_msgs/msg/PoseStamped.html)
* `/shift/odometry` -> [nav_msgs/msg/Odometry](https://docs.ros2.org/foxy/api/nav_msgs/msg/Odometry.html)
* `/shift/time_reference` -> [sensor_msgs/msg/TimeReference](https://docs.ros2.org/latest/api/sensor_msgs/msg/TimeReference.html)
* `/shift/tracked_objects` -> [outsight_shift_driver/msg/TrackedObjects](https://docs.ros2.org/latest/api/outsight_shift_driver/msg/TrackedObjects.html)
* `/shift/augmented_cloud` -> [outsight_shift_driver/msg/AugmentedCloud](https://docs.ros2.org/latest/api/outsight_shift_driver/msg/AugmentedCloud.html)
* `/shift/zones` -> [outsight_shift_driver/msg/Zones](https://docs.ros2.org/latest/api/outsight_shift_driver/msg/Zones.html)
* `/shift/gps_pose` -> [sensor_msgs/msg/NavSatFix](https://docs.ros2.org/latest/api/sensor_msgs/msg/NavSatFix.html)

#### Timestamps

Both Shift and ROS timestamps can be used to stamp the previous topic messages. The topic `/shift/time_reference` links the Shift timestamp and the ROS timestamp.
To use the Shift timestamp, set the `/use_shift_time` parameter to 'true' in the config file.

#### Provided transformation

If the transformation between the robot base frame and the sensor frame is provided (for instance within a URDF file), 
the package will broadcast the transform between the fixed frame (defined as the initial pose of the robot) and the robot base frame.
Otherwise, the transformation broadcasted is simply the transformation between the fixed frame and the sensor frame.
The tf broadcaster can be disabled by setting the `broadcast_tf` parameter to 'false' in the config file. 

The static transform between the map frame and fixed frame is always published. 
This transform is particularly useful in the case of relocalization in a reference map. 
This transform gives the link between the origin of the map and the first position where the processing starts. 
This parameter also sets the frame used for the Zones message.
When relocalization is disabled, `map_frame` and `fixed_frame` are the same.

With the default settings, the transform tree is structured like this:
            `map_frame -> fixed_frame -> base_frame -> sensor_frame`

#### Frames used for pose and odometry

For Pose, the coordinate frame used is:
- map frame, in case of Relocalization, or
- fixed frame, i.e. the initial Pose of the LiDAR, in case of Ego-Motion

In contrast, the odometry message, corresponding to the displacement between the first frame and the current frame, 
is always given in the fixed frame.  

### shift_services

This node defines services to interact with the ALB.

#### Custom services files

* [`outsight_shift_driver/srv/ShiftConfig`](https://docs.ros.org/en/melodic/api/outsight_shift_driver/html/srv/ShiftConfig.html)
  
  Service to manage the processing configuration of the ALB. Used for getting and putting the configuration from/to the ALB.

* [`outsight_shift_driver/srv/ShiftFile`](https://docs.ros.org/en/melodic/api/outsight_shift_driver/html/srv/ShiftFile.html)

  Service to use with the storage of the ALB, to download or upload files.

#### Services

For more information about the Shift API, please refer to the [API documentation](https://app.swaggerhub.com/apis-docs/Outsight/alb_api/)

* `/shift/processing/restart` -> [std_srvs/srv/Trigger](https://docs.ros2.org/latest/api/std_srvs/srv/Trigger.html)

  Service to restart the processing on the ALB. Trigger service does not require any arguments, you can just call it with the following command:
  ```sh
  ros2 service call /shift/processing/restart
  ```

* `/shift/processing/stop` -> [std_srvs/srv/Trigger](https://docs.ros2.org/latest/api/std_srvs/srv/Trigger.html)

  Service to stop the processing on the ALB.
  ```sh
  ros2 service call /shift/processing/stop
  ```

* `/shift/processing/get_config` -> [outsight_shift_driver/srv/ShiftConfig](https://docs.ros2.org/latest/api/outsight_shift_driver/srv/ShiftConfig.html)

  Service to get the processing configuration of the ALB. If argument is an empty string, configuration will be outputted in the ros2 launch console. If argument is a full filepath, the configuration will be exported to this file.

  WARNING: Filepath has to be absolute.

  ```sh
  ros2 service call /shift/processing/get_config ""
  ```
  ```sh
  ros2 service call /shift/processing/get_config "/home/username/Documents/out_config.json"
  ```

* `/shift/processing/put_config` -> [outsight_shift_driver/srv/ShiftConfig](https://docs.ros2.org/latest/api/outsight_shift_driver/srv/ShiftConfig.html)

  Service to put the processing configuration on the ALB. It has to be formatted as JSON format.

  ```sh
  ros2 service call /shift/processing/put_config "/home/username/Documents/config_to_put.json"
  ```

* `shift/storage/download` -> [outsight_shift_driver/srv/ShiftFile](https://docs.ros2.org/latest/api/outsight_shift_driver/srv/ShiftFile.html)

  Service to download a file from the ALB.
  ```sh
  # Call to download a map named map_name.ply on the ALB to the output file.
  ros2 service call /shift/storage/download maps map_name.ply "/home/username/Documents/output_map.ply"

  # Same command with full argument description.
  ros2 service call /shift/storage/download "category: 'maps' shift_filename: 'map_name.ply' filepath: '/home/username/Documents/output_map.ply'"
  ```

* `shift/storage/upload` -> [outsight_shift_driver/srv/ShiftFile](https://docs.ros.org/en/melodic/api/outsight_shift_driver/html/srv/ShiftFile.html)

  Service to upload a file to the ALB.
  ```sh
  # Call to upload a map named map_name.ply to the ALB.
  ros2 service call /shift/storage/upload maps map_name.ply "/home/username/Documents/input_map.ply"
  ```

* `shift/storage/list` -> [outsight_shift_driver/srv/ShiftFile](https://docs.ros.org/en/melodic/api/outsight_shift_driver/html/srv/ShiftFile.html)

  Service to list the files on the ALB. 
  
  By default, the response is printed into the rosconsole. Set a `filepath` to export the response to a file. Note that the parameter `shift_filename` isn't used here.
  ```sh
  # List the map files on the ALB.
  ros2 service call /shift/storage/list maps "" ""

  # List the map files on the ALB and export it.
  ros2 service call /shift/storage/list maps "" "/home/username/maps_storage.json"
  ```

## Usage with other ROS messages

### LaserScan

For some applications, instead of the `PointCloud`, `LaserScan` can be useful. For this, we recommend using the ROS package [pointcloud_to_laserscan](http://wiki.ros.org/pointcloud_to_laserscan).

Then you can launch in another console the given launch file `outsight_to_laserscan.launch`, adjusting your own parameters. You can also integrate it directly in your own launch file for easier use.

```sh
ros2 launch outsight_shift_driver outsight_to_laserscan.launch
```

## Unit testing

In this ROS driver, unit tests have been defined under the `test` directory. After any change, make sure the tests are still able to run.

```sh
colcon test
```

This driver has been tested in CI under `humble` and `iron` releases.

## Contributing
### Bugs and issues
Please report bugs and/or issues using the [Issue Tracker](https://gitlab.com/outsight-public/outsight-drivers/outsight_shift_ros2_driver/-/issues)

### Feature requests and additions
In order to contribute to the code, please use Merge requests.

In case of Merge requests, the code will be integrated internally, in order to execute Continuous Integration and Continuous Deployment before being deployed publicly.
